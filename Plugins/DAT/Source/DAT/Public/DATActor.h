/****************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations.  The intellectual and technical
 * concepts contained herein are proprietary to Nocturnal Innovations
 * and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the authorized form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API in combination with one or more example
 * projects. All other uses are strictly forbidden unless prior written
 * permission is obtained from Nocturnal Innovations.
 */

#pragma once

#include "DTEntity.h"
#include "DTPlayerController.h"
#include "DATActor.generated.h"

UENUM(BlueprintType)
enum class DTAuthorityEnum : uint8
{
	Authority,
	Remote,
	Client
};

UENUM(BlueprintType)
enum class DTMaterialityEnum : uint8
{
	Default,
	Enabled,
	Disabled
};

UINTERFACE(BlueprintType)
class DAT_API UDATActor : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};


class DAT_API IDATActor
{
	GENERATED_IINTERFACE_BODY()

public:
	IDATActor();

	/**
	 * Provides the entity that represents this actor in the cluster
	 * @return The entity that represents this actor
	 */
	DTEntity* DT_Entity();

	/**
	 * Indicates the (cluster-side) entity address for this actor
	 * @return The EntityAddr for this actor
	 */
	EntityAddr DT_Address() const;

	/**
	 * Provides the actor's component at a given ID
	 * @param ComponentID - The ID of the component to get (provided by DT_GetComponentID)
	 * @return The component at the given ID or NULL if there is no component with that ID
	 */
	UActorComponent* DT_GetComponent(int ComponentID) const;

	/**
	 * Provides the ID of a given component
	 * @param Component - The component to identify
	 * @return The ID of the given component or zero if it is not found
	 */
	int DT_GetComponentID(const UActorComponent* Component) const;

	/**
	 * Called by DAT to retrieve this actor's registered type
	 * Override this to provide DAT with your actor's type
	 * @return This actor's registered type
	 */
	virtual int DT_GetType() const;

	/**
	 * Called by DAT when the (local) actor should be initialized with the cluster
	 * Called right after AActor::BeginPlay()
	 * Override to implement your own functionality
	 */
	virtual void DT_Initialize();

	/**
	 * Called by DAT when the (remote) actor should duplicate its initial state from the cluster
	 * Called right before AActor::BeginPlay()
	 * Override to implement your own functionality
	 */
	virtual void DT_Duplicate();

	/**
	 * Called by DAT when the actor is spawned and active in the cluster
	 * Used to provide better timing between servers
	 */
	virtual void DT_Activate();

	/**
	 * Called by DAT when the (remote) actor should update its current state based on cluster data
	 * Called right before Tick()
	 * Override to implement your own functionality and (optionally) call base class version for movement
	 * @param ClusterUpdate - True if this function is called right after an update from the cluster, false if it is called on a tick between cluster updates
	 */
	virtual void DT_Reflect(bool ClusterUpdate);

	/**
	 * Called by DAT when the (local) actor should update its current state to the cluster
	 * Called right after AActor::Tick()
	 * Override to implement your own functionality
	 */
	virtual void DT_Update();

	/**
	 * Called by DAT when this actor should be visible in the game world
	 * Override to implement your own functionality
	 */
	virtual void DT_Show();

	/**
	 * Called by DAT when this actor should not be visible in the game world
	 * Override to implement your own functionality
	 */
	virtual void DT_Hide();

	/**
	 * Called by DAT when this actor should use physics/collision in the game world
	 * Override to implement your own functionality
	 */
	virtual void DT_Enable();

	/**
	 * Called by DAT when this actor should not use physics/collision in the game world
	 * Override to implement your own functionality
	 */
	virtual void DT_Disable();

	/**
	 * Called by DAT when the (local) actor should be destroyed
	 * Called right after AActor::EndPlay()
	 * Override to implement your own functionality
	 */
	virtual void DT_Destroy();

	/**
	 * Indicates the cluster time-stamp of this actor
	 * @return The time-stamp
	 */
	double DT_Stamp() const;

	/**
	 * Indicates the current cluster-time
	 * @return Current cluster-time
	 */
	double DT_ClusterTime() const;

	/**
	 * Prepare an event to be triggered
	 * Call this and then write functions before an event is triggered
	 * @param MessageLength - The size of the message data to be sent
	 */
	void DT_PrepareEvent(int MessageLength);

	/**
	 * Prepare a message for sending
	 * Call with write functions before a message is sent
	 * @param MessageLength - The size of the message data to be sent
	 */
	void DT_PrepareMessage(int MessageLength);

	/**
	 * Sends a message to another actor (only works with DAT actors)
	 * @param To - The actor to send the message to (abstracted to AActor because the seperate DAT classes are derived from that)
	 * @param MessageType - The message type
	 */
	void DT_SendMessage(AActor* To, int MessageType);

	/**
	 * Sends a message to a group of actors (only works with DAT actors)
	 * @param To - The actors to send the message to (abstracted to AActor because the seperate DAT classes are derived from that)
	 * @param MessageType - The message type
	 */
	void DT_SendGroupMessage(const TArray<AActor*>& To, int MessageType);

	/**
	 * Sends a message to all actors in a given radius around the location parameter (only works with DAT actors)
	 * @param Location - The location of the emission
	 * @param Radius - The radius of the emission
	 * @param MessageType - The message type
	 */
	void DT_EmitMessage(FVector Location, float Radius, int MessageType);

	/**
	 * Called by DAT when an actor sends this actor a message
	 * Override to implement your own functionality
	 * @param From - The actor that sent the message (cannot be guaranteed persistent beyond the scope of a given call)
	 * @param MessageType - The message type
	 */
	virtual void DT_HandleMessage(AActor* from, int messageType);

	/**
	 * Called by DAT on a (remote) actor when it should handle an event that was triggered
	 * Override to implement your own functionality
	 * @param EventID - The ID of the event that was triggered
	 */
	virtual void DT_HandleEvent(int EventID);

	/**
	 * Called by DAT on a (local) actor so it can respond to the force of gravity
	 * Override to implement your own functionality
	 * @param GravityVector - The force vector imposed on this actor by the configuration of masses in the space
	 */
	virtual void DT_HandleGravity(FVector GravityVector);

	/**
	 * Indicates that this actor should receive updates of what actors are within its awareness
	 * The feature is disabled by default to reduce processing
	 * This must be called in the constructor to have an effect
	 */
	void DT_EnableAwarenessUpdates();

	/**
	 * Called to allow the actor to indicate its awareness state
	 * This is a position and a scalar threshold (size / distance)
	 * @param Position - The position of the actor's awareness
	 * @param Detail - The scalar threshold that determines significance
	 */
	virtual void DT_GetAwarenessState(FVector& Position, float& Detail);

	/**
	 * Sets the minimum change in position that the cluster will consider as movement
	 */
	void DT_SetClusterLinearTolerance(float Tolerance);

	/**
	 * Sets the minimum change in rotation that the cluster will consider as movement
	 */
	void DT_SetClusterAngularTolerance(float Tolerance);

protected:
	/**
	 * Read message data
	 * Data is already prepared, DO NOT call DT_PrepareMessage first
	 * @param MessageData - The data buffer to read into
	 * @param MessageSize - The size of the data buffer to read into
	 */
	void DT_ReadData(byte* MessageData, int MessageSize);

	/**
	 * Write message data
	 * Must call DT_PrepareMessage first
	 * @param MessageData - The data to write
	 * @param MessageSize - The size of the data to write
	 */
	void DT_WriteData(const byte* MessageData, int MessageSize);

	/**
	 * Read a contiguous data object (primitives and structs)
	 * Data is already prepared, DO NOT call DT_PrepareMessage first
	 * @param T - The type of the object to be read
	 * @param Obj - The object to read into
	 */
	template <class T>
	void DT_ReadObject(T& Obj)
	{
		const int len = sizeof(T);

		Obj = *((T*) (inData + inCur));
		inCur += len;
	}

	/**
	 * Write a contiguous data object (primitives and structs)
	 * Must call DT_PrepareMessage first
	 * @param T - The type of the object to be written
	 * @param Obj - The object to write
	 */
	template <class T>
	void DT_WriteObject(const T& obj)
	{
		const int len = sizeof(T);

		*((T*) (outData + outCur)) = obj;
		outCur += len;
	}

	/**
	 * Read an FString
	 * Data is already prepared, DO NOT call DT_PrepareMessage first
	 * @param String - The FString to read into
	 */
	void DT_ReadString(FString& String);

	int DT_StringLen(const FString& String) const;

	/**
	 * Write an FString
	 * Must call DT_PrepareMessage first
	 * @param String - The FString to write
	 */
	void DT_WriteString(const FString& String);

public:
	/******************************
	 * Internal
	 */
	int baseType;

	friend DTEntity;
	friend ADTGameMode;
	friend ADTActor;
	friend ADTPawn;
	friend ADTCharacter;
	friend ADTWheeledVehicle;

private:
	virtual void CallHandleVisibility(bool Visible);
	virtual void CallHandleMateriality(bool Material);

	bool DA_GatewayMode() const;
	bool DA_WorkerMode() const;
	bool DA_StandaloneMode() const;
	int DA_ClusterID() const;
	ADTGameMode* DA_GameMode() const;
	bool DA_HasAuthority() const;
	bool DA_IsRemoteServer() const;
	void DA_SetLocation(FVector Position);
	void DA_SetBoundingBox(FBox BoundingBox);
	void DA_SetBoundingSphere(float Radius);
	void DA_SetBoundingCapsule(float Radius, float HalfHeight, float CenterOffset);
	void DA_SetVisibilityScalar(float VisibilityScalar);
	void DA_SetMinimumPadding(float Padding);
	void DA_SetRotation(FRotator Rotation);
	void DA_SetLinearVelocity(FVector Delta);
	void DA_SetAngularVelocity(FVector AngularDelta);
	void DA_SetDensity(float Density);
	void DA_SetMass(float Mass);
	void DA_SetScale(FVector Scale);
	void DA_SetLuminosity(float Luminosity);
	FVector DA_Location() const;
	FRotator DA_Rotation() const;
	FVector DA_LocationWithEstimation() const;
	FRotator DA_RotationWithEstimation() const;
	FVector DA_Scale() const;
	FVector DA_LinearVelocity() const;
	FVector DA_AngularVelocity() const;
	float DA_Mass() const;
	float DA_Luminosity() const;
	float DA_Elapsed() const;
	bool DA_HasEntity() const;
	bool DA_IsVisible() const;
	bool DA_IsEnabled() const;
	void DA_SetVisible(bool Visible, bool AffectLocal = false);
	void DA_SetMaterial(DTMaterialityEnum State);
	void DA_TriggerEvent(int EventID, bool Immediate);
	bool DA_InCluster() const;

	static void DA_SwitchHasAuthority(const IDATActor* Target, DTAuthorityEnum& Branches);
	static bool DA_HasAuthority(const IDATActor* Target);

	void HandleEvent(int eventType, byte* data, int dataSz);
	void HandleMessage(AActor* from, int messageType, byte* data, int dataSz);
	double SCTime() const;
	double CCTime() const;


	byte* outData;
	int		outSize;
	int		outCur;
	byte* inData;
	int		inSize;
	int		inCur;
};
