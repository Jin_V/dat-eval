/****************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations.  The intellectual and technical
 * concepts contained herein are proprietary to Nocturnal Innovations
 * and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the authorized form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API in combination with one or more example
 * projects. All other uses are strictly forbidden unless prior written
 * permission is obtained from Nocturnal Innovations.
 */

#pragma once

#include "DTDefs.h"
#include "GameFramework/GameMode.h"
#include "DTGameMode.generated.h"

UENUM(BlueprintType)
enum class DTRoleEnum : uint8
{
	Master,
	Worker,
	Gateway,
	Standalone
};

class IDATActor;
class ADTPlayerController;


/**
 * ADTGameMode
 * The DAT game mode class
 * Sets up and manages cluster communications
 */
UCLASS(AutoExpandCategories=("DAT|DTGameMode","DAT|DTGameMode|Master"))
class DAT_API ADTGameMode : public AGameMode
{
  GENERATED_BODY()

public:
	ADTGameMode();

	/**
	 * Tells DAT to override the default role when running a node in the editor
	 * Editor-specific feature
	 * Used with RunInEditorAs (see below)
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DAT|DTGameMode")
	bool bOverrideRoleInEditor;

	/**
	 * Indicates what type of DAT node to run as (Master, Worker, or Gateway)
	 * Editor-specific feature
	 * OverrideRoleInEditor must be true for this setting to have an effect
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DAT|DTGameMode", Meta = (EditCondition="bOverrideRoleInEditor"))
	DTRoleEnum RunInEditorAs;

	/**
	 * Disables the warning that actors are being spawned on the Master node
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DAT|DTGameMode")
	bool bDisableSpawnOnMasterWarning;

	/**
	 * Address and port of the DAT Master node
	 * format: ip-address:port
	 * ex:     127.0.0.1:6666
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode")
	FString MasterHost;

	/**
	 * Desired milliseconds between cluster updates
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode")
	float UpdateWaitMS;

	/**
	 * Tells the node to use a specific IP for the DAT incoming socket
	 * Otherwise it will auto-detect the local IP via UE4
	 * Used with IncomingIP (see below)
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DAT|DTGameMode")
	bool bOverrideIncomingIP;

	/**
	 * The IP to use for the DAT incoming socket
	 * bOverrideIncomingIP must be true for this setting to have an effect
	 * format: ip-address
	 * ex:     127.0.0.1
	 */
	UPROPERTY(EditAnywhere, Category = "DAT|DTGameMode", Meta = (EditCondition="bOverrideIncomingIP"))
	FString IncomingIP;

	/**
	 * Tells the node to use a specific IP for the DAT outgoing socket
	 * Otherwise it will auto-detect the local IP via UE4
	 * Used with OutgoingIP (see below)
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DAT|DTGameMode")
	bool bOverrideOutgoingIP;

	/**
	 * The IP to use for the DAT outgoing socket
	 * bOverrideOutgoingIP must be true for this setting to have an effect
	 * format: ip-address
	 * ex:     127.0.0.1
	 */
	UPROPERTY(EditAnywhere, Category = "DAT|DTGameMode", Meta = (EditCondition="bOverrideOutgoingIP"))
	FString OutgoingIP;

	/**
	 * Tells the node to bind the DAT outgoing socket to a specific local IP
	 * Otherwise it will auto-detect the local IP via UE4
	 * Used with OutgoingIP (see below)
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "DAT|DTGameMode")
	bool bOverrideLocalIP;

	/**
	 * The local IP (NIC) to for DAT to bind to (for multicast)
	 * bOverrideLocalIP must be true for this setting to have an effect
	 * format: ip-address
	 * ex:     127.0.0.1
	 */
	UPROPERTY(EditAnywhere, Category = "DAT|DTGameMode", Meta = (EditCondition="bOverrideOutgoingIP"))
	FString LocalIP;

	/**
	 * The base port for input cluster sockets
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode")
	int32 ClusterInPortBase;

	/**
	 * The base port for output cluster sockets
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode")
	int32 ClusterOutPortBase;

	/**
	 * The port to listen on for incoming ping connections
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode")
	int32 PingListenPort;

	/**
	 * Tells the Master node how many DOT nodes to expect in the cluster
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode|Master")
	int DOT_Count;

	/**
	 * Tells the Master node how many DAT (UE4 server) nodes to expect in the cluster
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode|Master")
	int DAT_Count;

	/**
	 * Enables universal gravity feature (experimental)
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode|Master")
	bool bUseGravity;

	/**
	 * Enables lighting support (semi-experimental)
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode|Master")
	bool bUseLighting;

	/**
	 * Enables multicast for cluster communications (outgoing data socket only)
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode|Master")
	bool bUseMulticast;

	/**
	 * Multicast rate for outgoing data sockets
	 * Only used if UseMulticast is true
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode|Master", Meta = (EditCondition="UseMulticast"))
	int32 MulticastRate;

	/**
	 * Size of the space to simulate
	 * Represented as the length of one edge of a cube (space dimensions are size^3)
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode|Master")
	float SpaceSize;

	/**
	 * Maximum actor size that will be put into the space (as a radius)
	 * Should be as small as possible
	 * 1/20 (or less) of SpaceSize is recommended
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode|Master")
	float MaxActorSize;

	/**
	 * The (linearized) maximum actor mass that will be put into the space
	 * 'Linaerized' means this value is actually the cube-root of the maximum mass allowed
	 * Should be as small as possible
	 * Only used at initialization, changing it after will not affect the game world
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode|Master")
	float MaxActorMass;

	/**
	 * Scalar threshold that determines when individual masses are significant
	 * Larger values should improve cluster performance at the cost of simulation accuracy
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode|Master")
	float GravityThreshold;

	/**
	 * Internal octree parameter - change at your own risk
	 * Object count at which point sub-cells merge into their parent
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode|Master|OSpace")
	int MinThresh;
	
	/**
	 * Internal octree parameter - change at your own risk
	 * Object count at which point a cell divides into sub-cells
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode|Master|OSpace")
	int MaxThresh;
	
	/**
	 * Internal octree parameter - change at your own risk
	 * Absolute maximum depth of the tree
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode|Master|OSpace")
	int MaxDepth;

	/**
	 * Internal octree parameter - change at your own risk
	 * Tweaking this may help DOT performance, but it's probably best to leave this one alone
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DAT|DTGameMode|Master|OSpace")
	float FudgeFactor;

  /**
	 * Actual number of server updates per second
	 * (Diagnostic value)
	 */
  UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
	float DT_UpdatesPerSecond();

  /**
	 * Actual time between server updates (in seconds)
	 * (Diagnostic value)
	 */
  UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
	float DT_SecondsPerUpdate();

  /**
	 * Total number of objects being hosted locally
	 * (Diagnostic value)
	 */
  UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
  int32 DT_NumLocalObjects();

  /**
	 * Total number of remote objects being replicated
	 * (Diagnostic value)
	 */
  UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
  int32 DT_NumLoadedObjects();

  /**
	 * Total number of objects using physics
	 * (Diagnostic value)
	 */
  UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
  int32 DT_NumActiveObjects();

  /**
	 * Total number of remote objects visible to aware local objects
	 * (Diagnostic value)
	 */
  UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
  int32 DT_NumVisibleObjects();

	/**
	 * Is this node connected to the cluster?
	 * @return True if the engine is in the cluster, false if not
	 */
  UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
  bool DT_InCluster();
  
	/**
	 * Is this node the Master node?
	 * @return True if the engine is the Master, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
	bool DT_MasterMode();

	/**
	 * Is this node a Gateway node?
	 * @return True if the engine is a Gateway, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
	bool DT_GatewayMode();

	/**
	 * Is this node a Worker node?
	 * @return True if this node is a Worker, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
	bool DT_WorkerMode();

	/**
	 * Is this server/client in Standalone mode?
	 * @return True if the engine is Standalone, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
	bool DT_StandaloneMode();

	/**
	 * Called by user code after actor types have been registered
	 */
  UFUNCTION(BlueprintCallable, Category = "DAT|DTGameMode")
  void DT_Ready();

	/**
	 * Is this node connected and running in the cluster?
	 * @return True if the engine is connected and running, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
	bool DT_Running();

	/**
	 * Indicates the number of nodes in the cluster (including Master)
	 * @return The number of UE4 nodes in the cluster
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
	int DT_NumNodes();

	/**
	 * Indicates the number of Worker nodes in the cluster (including self, if called by a Worker)
	 * @return The number of Workers in the cluster
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
	int DT_NumWorkers();

	/**
	 * Indicates if a node is a Worker
	 * @param NodeID - The ID of the node to test
	 * @return True if the node at NodeID is a Worker, false otherwise
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
	bool DT_IsWorker(int NodeID);

	/**
	 * Indicates the cluster ID of this node
	 * @return This node's cluster ID
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
	int DT_ClusterID();

	/**
	 * Indicates the size of the space being simulated by the cluster (see SpaceSize)
	 * @return The size of the space
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
	float DT_SpaceSize();

	/**
	 * Indicates the maximum size of objects allowed in the simulation space (see MaxActorSize)
	 * This should be used on worker and gateway nodes as MaxActorSize is only used on the master node
	 * @return The maximum size of objects allowed in the simulation space
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
	float DT_MaxSize();

	/**
	 * Indicates the current time within the cluster (same on all nodes)
	 * @return Current cluster time
	 */
  UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
  float DT_Time();

  /**
	 * Indicates the actual time between updates sent to the cluster
	 * @return Actual milliseconds between updates
	 */
  UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
  float DT_UpdateRate();

	/**
	 * Blueprint event which indicates to user code that it is safe to build the world (spawn objects)
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTGameMode")
	void DT_SetupWorld();

	/**
	 * Called to query the cluster-role of the executing node
	 * @param Target - The ADTGameMode to test (hidden in blueprints)
	 * @param OutBranches - Upon return, holds the role of this node in the cluster (Master, Worker, or Gateway)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTGameMode", Meta = (BlueprintProtected, ExpandEnumAsExecs = "OutBranches", HidePin = "Target", DefaultToSelf = "Target"))
	static void DT_SwitchOnRole(ADTGameMode* Target, DTRoleEnum& OutBranches);

	/**
	 * Indicates if the actor is the Authority (not a client or remote server)
	 * @param Target - The actor to test
	 * @return True if the target actor has authority, false otherwise
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
	static bool DT_HasAuthority(AActor* Target);

	/**
	 * Indicates that the actor is a valid DAT class
	 * @param Target - The actor to test
	 * @return True if the target actor is a valid DAT class, false otherwise
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTGameMode")
	static bool DT_IsDATActor(AActor* Target);

	/**
	 * Provides Master with a list of IDs for the workers that can host a given actor type
	 * @param TypeName - The name of the type that is intended to be spawned
	 * @param Workers - An array of worker IDs
	 * @param Success - Indicates that the process succeeded
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTGameMode")
	void DT_GetWorkersForType(FString TypeName, TArray<int32>& Workers, bool& Success);

	/**
	 * Prepare a spawn command for sending to workers
	 * Call with write functions before a spawn command is sent
	 * @param DataLength - The size of the spawn data to be sent
	 */
	void DT_PrepareSpawn(int DataLength);

	/**
	 * Prepare a multi-spawn command for sending to workers
	 * Call with write functions before a spawn command is sent
	 * @param DataLength - The size of the spawn data to be sent
	 * @param SpawnCount - The number of spawns to be sent
	 */
	void DT_PrepareMultiSpawn(int DataLength, int SpawnCount);

	/**
	 * Allows Master to spawn actors on a specific worker node
	 * Must call DT_PrepareSpawn first
	 * Optionally write spawn data using DT_WriteData() and/or DT_WriteObject()
	 * @param WorkerID - The ID of the worker node to spawn on (from 0 to (DT_NumWorkers() - 1))
	 * @param EntityType - The ID of the type of entity to spawn
	 */
	void DT_SpawnOnWorker(int WorkerID, int EntityType);

	/**
	 * Allows Master to spawn actors on a specific worker node
	 * Must call DT_PrepareSpawn first
	 * Optionally write spawn data using DT_WriteData() and/or DT_WriteObject()
	 * @param WorkerID - The ID of the worker node to spawn on (from 0 to (DT_NumWorkers() - 1))
	 * @param EntityType - The ID of the type of entity to spawn
	 */
	void DT_SpawnMultiOnWorker(int WorkerID, int EntityType);

	/**
	 * Allows Master to spawn actors evenly across the cluster
	 * Must call DT_PrepareSpawn first
	 * Optionally write spawn data using DT_WriteData() and/or DT_WriteObject()
	 * @param EntityType - The ID of the type of entity to spawn
	 */
	void DT_DistributedSpawn(int EntityType);

	/**
	 * Allows Master to spawn multiple actors evenly across the cluster
	 * Must call DT_PrepareSpawn first
	 * Optionally write spawn data using DT_WriteData() and/or DT_WriteObject()
	 * @param EntityType - The ID of the type of entity to spawn
	 */
	void DT_DistributedMultiSpawn(int EntityType);

	/**
	 * Workers implement this to handle spawn requests from the Master
	 * Spawn data is already prepared, DO NOT call DT_PrepareSpawn first
	 * Optionally read spawn data using DT_ReadData() and/or DT_ReadObject()
	 * @param EntityType - The ID of the type of entity to spawn
	 */
	virtual void DT_HandleSpawn(int EntityType);

	/**
	 * Attempts to find a DTEntity by its EntityAddr
	 * @param EntityAddress - The EntityAddr of the entity to find
	 * @return The DTEntity if it was found, NULL otherwise
	 */
	DTEntity* DT_FindEntity(EntityAddr EntityAddress);

	/**
	 * Attempts to find a DAT actor by its EntityAddr
	 * @param EntityAddress - The EntityAddr of the actor to find
	 * @return The actor if it was found, NULL otherwise
	 */
	AActor* DT_FindActor(EntityAddr EntityAddress);

	/**
	 * Connection Result callback
	 * Implement this to receive cluster-connection results
	 * @param Result - The Result of the connection attempt
	 */
  virtual void DT_ConnectResult(DTConnResult Result);

	/**
	 * Called to allow user code to initialize this game mode 
	 * DAT calls this after cluster is ready but before the DT_SetupWorld event
	 */
	virtual void DT_Initialize();

	/**
	 * Called to allow user code to update all local entities across the cluster
	 */
	virtual void DT_UpdateEntities();

	/**
	 * Moves all actors of a single type (collisions only)
	 * @param EntityType - The DTType to process for movement
	 */
	void DT_Move(DTType* EntityType);

	/**
	 * Moves all actors of a single type (collisions and visibility)
	 * @param EntityType - The DTType to process for movement and visibility
	 */
	void DT_MoveView(DTType* EntityType);

	/**
	 * Prepare a message for sending
	 * Call with write functions before a message is sent
	 * @param MessageLength - The size of the message data to be sent
	 */
	void DT_PrepareMessage(int MessageLength);

	/**
	 * Send a message to the Master node
	 * Must call DT_PrepareMessage first
	 * Optionally write message data using DT_WriteData() and/or DT_WriteObject()
	 * @param MessageType - The message type
	 */
	void DT_SendMasterMessage(int MessageType);

	/**
	 * Send a message to any node from the Master
	 * Must call DT_PrepareMessage first
	 * Optionally write message data using DT_WriteData() and/or DT_WriteObject()
	 * @param ToNodeID - The ID of the node to send to
	 * @param MessageType - The message type
	 */
	void DT_SendMasterMessage(int ToNodeID, int MessageType);

	/**
	 * Send a global message to all nodes (including self)
	 * Must call DT_PrepareMessage first
	 * Optionally write message data using DT_WriteData() and/or DT_WriteObject()
	 * @param MessageType - The message type
	 */
	void DT_SendGlobalMessage(int MessageType);

	/**
	 * Override this to implement custom Master messages
	 * Message data is already prepared, DO NOT call DT_PrepareMessage first
	 * Optionally read message data using DT_ReadData() and/or DT_ReadObject()
	 * @param FromNodeID - The ID of the node that sent the message
	 * @param MessageType - The message type
	 */
  virtual void DT_HandleMasterMessage(int FromNodeID, int MessageType);

#ifndef DAT_CLIENT_MODE
	/**
	 * Creates a new DAT type
	 * @param Type - The ID of this type
	 * @param ActorClassName - Name of the UE4 actor class for this type
	 * @param EntityClassName - Name of the DAT entity class for this type
	 * @param LocalBundleSize - The maximum number of actors in each bundle
	 * @param ActorUClass - The UE4 UClass object for this actor type
	 * @param Behavior - Enum indicating how this type should behave (CollisionOnly or CollisionAndVision)
	 */
	template <int Type, class ActorClassName, class EntityClassName>
	DTType* DAT_TYPE(char const* Name, int LocalBundleSize, UClass* ActorUClass, DTBehavior Behavior)
	{
		return new DTType_Impl<EntityClassName, ActorClassName>(this, Name, (int) Type, LocalBundleSize, ActorUClass, Behavior);
	}
#else
	/**
	 * Does nothing on client, returns NULL
	 * @return NULL
	 */
	template <int Type, class ActorClassName, class EntityClassName>
	DTType* DAT_TYPE(char const* Name, int LocalBundleSize, UClass* ActorUClass, int BehaviorFlags)
	{
		return NULL;
	}
#endif

protected:
	/**
	 * Read message data
	 * Data is already prepared, DO NOT call DT_PrepareMessage first
	 * @param MessageData - The data buffer to read into
	 * @param MessageSize - The size of the data buffer to read into
	 */
	void DT_ReadData(byte* MessageData, int MessageSize);

	/**
	 * Write message data
	 * Must call DT_PrepareMessage first
	 * @param MessageData - The data to write
	 * @param MessageSize - The size of the data to write
	 */
	void DT_WriteData(const byte* MessageData, int MessageSize);

	/**
	 * Read a contiguous data object (primitives and structs)
	 * Data is already prepared, DO NOT call DT_PrepareMessage first
	 * @param T - The type of the object to be read
	 * @param Obj - The object to read into
	 */
	template <class T>
	void DT_ReadObject(T& Obj)
	{
		const int len = sizeof(T);

		Obj = *((T*) (inData + inCur));
		inCur += len;
	}

	/**
	 * Write a contiguous data object (primitives and structs)
	 * Must call DT_PrepareMessage first
	 * @param T - The type of the object to be written
	 * @param Obj - The object to write
	 */
	template <class T>
	void DT_WriteObject(const T& obj)
	{
		const int len = sizeof(T);

		*((T*) (outData + outCur)) = obj;
		outCur += len;
	}

	/**
	 * Read an FString
	 * Data is already prepared, DO NOT call DT_PrepareMessage first
	 * @param String - The FString to read into
	 */
	void DT_ReadString(FString& String);

	int DT_StringLen(const FString& String) const;

	/**
	 * Write an FString
	 * Must call DT_PrepareMessage first
	 * @param String - The FString to write
	 */
	void DT_WriteString(const FString& String);

	/**
	 * Same as PrintString -- but at a C++ level so it will print in Shipping builds if we use the bUseLoggingInShipping flag in *.Target.cs
	 * @param String - The string to print to the log
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTGameMode")
	static void DT_PrintString(const FString& String);

private:
	void HandleSpawn(int entityType, byte* data, int dataSz);

	byte* outData;
	int		outSize;
	int		outCur;
	byte* inData;
	int		inSize;
	int		inCur;
	int		spawnCount;
	int		spawnSize;

public:
	static double StartupTime;
};
