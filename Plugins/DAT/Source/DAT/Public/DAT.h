/****************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations.  The intellectual and technical
 * concepts contained herein are proprietary to Nocturnal Innovations
 * and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the authorized form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API in combination with one or more example
 * projects. All other uses are strictly forbidden unless prior written
 * permission is obtained from Nocturnal Innovations.
 */

#pragma once

#include "ModuleManager.h"

class DTComm;

class DAT_API FDATModule : public IModuleInterface
{
public:

	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	static inline bool IsAvailable()
	{
		return FModuleManager::Get().IsModuleLoaded("DAT");
	}

	static inline FDATModule& Get()
	{
		return FModuleManager::LoadModuleChecked<FDATModule>("DAT");
	}
};
