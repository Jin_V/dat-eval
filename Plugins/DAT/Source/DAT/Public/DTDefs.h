/****************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations.  The intellectual and technical
 * concepts contained herein are proprietary to Nocturnal Innovations
 * and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the authorized form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API in combination with one or more example
 * projects. All other uses are strictly forbidden unless prior written
 * permission is obtained from Nocturnal Innovations.
 */

#pragma once

#include "Containers/Queue.h"
#include "HAL/ThreadingBase.h"

class DTEntity;
class ADTActor;
class ADTCharacter;
class ADTPawn;
class ADTWheeledVehicle;
class ADTGameMode;
class DTType;
class FDATModule;

typedef unsigned char byte;

typedef enum
{
	Connected,
	InCluster,
	TimedOut,
	Refused,
	Disconnected,
	Terminated,
	HandshakeError
} DTConnResult;

typedef enum
{
	CollisionOnly,
	CollisionAndVision
} DTBehavior;

typedef struct EntityAddr
{
	int cID;
	int bID;
	int oID;

	EntityAddr()
	{
		cID = -1;
		bID = 0;
		oID = 0;
	}

	EntityAddr(int c, int b, int o)
	{
		cID = c;
		bID = b;
		oID = o;
	}

	bool operator==(const struct EntityAddr& B) const
	{
		if(cID != B.cID)
			return false;
		if(bID != B.bID)
			return false;
		if(oID != B.oID)
			return false;
		return true;
	}

	bool IsValid()
	{
		return cID != -1;
	}

	static EntityAddr InvalidAddr()
	{
		return EntityAddr();
	}

	friend FORCEINLINE uint32 GetTypeHash(const struct EntityAddr& ba)
	{
		uint32 hash = 17;

		hash = hash * 23 + ba.oID;
		hash = hash * 23 + ba.bID;
		hash = hash * 23 + ba.cID;
		return (uint32) hash;
	}
} EntityAddr;

#define DT_MESSAGE_BASE 100
#define DT_MESSAGE(x) (x + DT_MESSAGE_BASE)
