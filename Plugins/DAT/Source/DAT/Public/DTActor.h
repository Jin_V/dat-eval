/****************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations.  The intellectual and technical
 * concepts contained herein are proprietary to Nocturnal Innovations
 * and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the authorized form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API in combination with one or more example
 * projects. All other uses are strictly forbidden unless prior written
 * permission is obtained from Nocturnal Innovations.
 */

#pragma once

#include "DATActor.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Character.h"
#include "UnrealNetwork.h"
#include "LogMacros.h"
#include "WheeledVehicle.h"

#include "DTActor.generated.h"


USTRUCT()
struct FPStruct
{
	GENERATED_BODY()

	FPStruct(FVector& delta, FVector& angDelta, double stamp)
	{
		LinearVelocity = delta;
		AngularVelocity = angDelta;
		Stamp = stamp;
	}

	FPStruct() { }

	UPROPERTY()
	FVector Location;
	UPROPERTY()
	FRotator Rotation;
	UPROPERTY()
	FVector LinearVelocity;
	UPROPERTY()
	FVector AngularVelocity;
	UPROPERTY()
	double Stamp;
};

/*
 * ADTActor
 * The DAT actor class
 * Part of the DAT API for UE4
 */
UCLASS(AutoExpandCategories=("DAT|DTActor"))
class DAT_API ADTActor : public AActor, public IDATActor
{
  GENERATED_BODY()

public:
  ADTActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	/**
	 * Sets the actor's position in the DAT cluster
	 * @param Position - The position to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetLocation(FVector Position);

	/**
	 * Sets the actor's extents
	 * Use this, DT_SetBoundingSphere, or DT_SetBoundingCapsule, but only one as they override each other
	 * @param BoundingBox - The bounding box to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetBoundingBox(FBox BoundingBox);

	/**
	 * Sets the actor's radius
	 * Use this, DT_SetBoundingBox, or DT_SetBoundingCapsule, but only one as they override each other
	 * @param Radius - The radius to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetBoundingSphere(float Radius);

	/**
	 * Sets the actor's radius
	 * Use this, DT_SetBoundingBox, or DT_SetBoundingSphere, but only one as they override each other
	 * @param Radius - The radius to use
	 * @param HalfHeight - The half-height to use
	 * @param CenterOffset - The center offset to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetBoundingCapsule(float Radius, float HalfHeight, float CenterOffset);

	/**
	 * Sets the actor's minimum padding
	 * Use this to set a minimum collision buffer around an actor
	 * @param Padding -The padding to use (set to -1.0 to go back to default padding)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetMinimumPadding(float Padding);

	/**
	 * Sets the scalar threshold which determines when objects are significant enough to be visible (replicated)
	 * @param VisibilityScalar - The scalar to use (calculated as radius divided by distance)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetVisibilityScalar(float VisibilityScalar);

	/**
	 * Sets the actor's rotation
	 * @param Rotation - The rotation to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetRotation(FRotator Rotation);

	/**
	 * Sets the actor's linear velocity
	 * @param Delta - The velocity vector to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetLinearVelocity(FVector Delta);

	/**
	 * Sets the actor's angular velocity
	 * @param AngularDelta - The angular velocity vector to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetAngularVelocity(FVector AngularDelta);

	/**
	 * Sets the actor's density (computes mass given general spherical volume)
	 * @param Density - The density to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetDensity(float Density);

	/**
	 * Sets the actor's mass
	 * @param Mass - The mass to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetMass(float Mass);

	/**
	 * Sets the actor's scale
	 * @param Scale - The scale to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetScale(FVector Scale);

	/**
	 * Sets the actor's luminosity (light radius)
	 * Lighting feature must be enabled on the Master node
	 * @param Luminosity - The radius of the sphere of light around the actor
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetLuminosity(float Luminosity);

	/**
	 * Provides the current game mode
	 * @return The current game mode
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor")
	ADTGameMode* DT_GameMode() const;

	/**
	 * Indicates the actor's current position
	 * @return The actor's current position
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor")
	FVector DT_Location() const;

	/**
	 * Indicates the actor's current rotation
	 * @return The actor's current rotation
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor")
	FRotator DT_Rotation() const;

	/**
	 * Indicates the actor's current position including estimation based on linear velocity
	 * @return The actor's current position with estimation
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor")
	FVector DT_LocationWithEstimation() const;

	/**
	 * Indicates the actor's current rotation including estimation based on angular velocity
	 * @return The actor's current rotation with estimation
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor")
	FRotator DT_RotationWithEstimation() const;

	/**
	 * Indicates the actor's current scale
	 * @return The actor's current scale
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor")
	FVector DT_Scale() const;

	/**
	 * Indicates the actor's current velocity vector
	 * @return The actor's current velocity vector
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor")
	FVector DT_LinearVelocity() const;

	/**
	 * Indicates the actor's current angular velocity vector
	 * @return The actor's current angular velocity vector
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor")
	FVector DT_AngularVelocity() const;

	/**
	 * Indicates the actor's mass
	 * @return The actor's mass
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor")
	float DT_Mass() const;

	/**
	 * Indicates the actor's current luminosity (light radius)
	 * @return The radius of the sphere of light around the actor
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor")
	float DT_Luminosity() const;

	/**
	 * Provides the time (in seconds) since this actor was last updated
	 * Applies to remote actors
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor")
	float DT_Elapsed() const;

	/**
	 * Called when the blueprint should handle actor visibility
	 * @param Visible - True indicates visible, false indicates hidden
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTActor")
	void DT_SetVisibility(bool Visible);

	/**
	 * Called when the blueprint should enable/disable actor's physics/collision
	 * @param Material - True indicates physical, false indicates ephemeral
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTActor")
	void DT_SetMateriality(bool Material);

	/**
	 * Handle awareness-add updates, if needed
	 * Allows the actor to keep a list of actors it is aware of
	 * @param Actors - The list of new actors in this actor's awareness
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTActor")
	void DT_HandleAddToAwareness(const TArray<AActor*>& Actors);

	/**
	 * Handle awareness-remove updates, if needed
	 * Allows the actor to keep a list of actors it is aware of
	 * @param Actors - The list of actors no longer in this actor's awareness
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTActor")
	void DT_HandleRemoveFromAwareness(const TArray<AActor*>& Actors);

  /**
	 * Handle awareness updates, if needed
	 * @param Actors - The complete list of all actors this actor is currently aware of
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTActor")
	void DT_HandleAwareness(const TArray<AActor*>& Actors);

	/**
	 * Is this actor being represented in the cluster space?
	 * @return True if this actor is in the cluster, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor")
	bool DT_InCluster() const;

	/**
	 * Is this actor running on a gateway?
	 * @return True if this actor is running on a gateway, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor")
	bool DT_GatewayMode() const;

	/**
	 * Is this actor running on a worker?
	 * @return True if this actor is running on a worker, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor")
	bool DT_WorkerMode() const;

	/**
	 * Is this actor running on a standalone instance?
	 * @return True if this actor is running on a standalone instance, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor")
	bool DT_StandaloneMode() const;

	/**
	 * Indicates the cluster ID of the node simulating this actor
	 * @return This node's cluster ID
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor")
	int DT_ClusterID() const;

	/**
	 * Indicates which kind of actor this is (Authority, Remote, Client)
	 * Similar to HasAuthority() but adds the Remote type
	 * Remote means the actor is a local representation of an actor that is running on another node
	 * @param Target - The DTActor to test (hidden in blueprints)
	 * @param OutBranches - Upon return, holds this actor's type (Authority, Remote, Client)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor", Meta = (BlueprintProtected, ExpandEnumAsExecs = "OutBranches", HidePin = "Target", DefaultToSelf = "Target"))
	static void DT_SwitchHasAuthority(const ADTActor* Target, DTAuthorityEnum& OutBranches);

	/**
	 * Indicates if the actor is the Authority (not a client or remote server)
	 * @param Target - The actor to test
	 * @return True if the target actor has authority, false otherwise
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTActor", Meta = (BlueprintProtected, HidePin = "Target", DefaultToSelf = "Target"))
	static bool DT_HasAuthority(const ADTActor* Target);

	/**
	 * Turns on/off the visibility of the actor across the entire cluster
	 * @param Visible - True to show, false to hide
	 * @param AffectLocal - True to show/hide locally, false to only affect remote representations
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetVisible(bool Visible, bool AffectLocal);

	/**
	 * Turns on/off the collision and/or physics of the actor across the entire cluster
	 * Overrides the default state, call this with Default as the option to reset it
	 * @param State - Enabled to make collidable, Disabled to make insubstantial, Default to let DAT handle it
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetMaterial(DTMaterialityEnum State);

	/**
	 * Triggers an event on a (local) actor that will propagate to its remote representations
	 * @param EventID - The ID of the event to trigger
	 * @param Immediate - Send event immediately (don't wait for replicated variables to be synced)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_TriggerEvent(int EventID, bool Immediate = false);

	/**
	 * Indicates that this actor should have its motion smoothed on the clients
	 * Smoothing depends on DT_LinearVelocity()
	 * DAT client-side smoothing can conflict with some actors that use their own client-side smoothing (i.e. ACharacter)
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "DAT|DTActor")
	bool bUseClientSmoothing;

	/**
	 * Indicates that this actor should not use physics at all on the clients
	 * DAT motion smoothing toggles physics on and off based on need (when materiality is not overridden)
	 * This option disables physics on the clients entirely but will still generate enable/disable materiality events (for collisions)
	 * Setting this to true may help client-side performance, but the side-effect is over-penetration during collisions
	 * Ignored when bUseClientSmoothing is false
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "DAT|DTActor", meta = (EditCondition = "bUseClientSmoothing"))
	bool bNoClientPhysics;

	/**
	 * Indicates that this actor should sweep movement on remote servers and clients (on local do it yourself)
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "DAT|DTActor")
	bool bSweepMovement;

	/**
	 * Indicates if this actor is the Authority (not a client or remote server)
	 * @return True if this target actor has authority, false otherwise
	 */
	bool DT_HasAuthority() const;

	/**
	 * Indicates if this actor is a remote server representation (not a client or authority server)
	 * @return True if this target actor has authority, false otherwise
	 */
	bool DT_IsRemoteServer() const;

	virtual void Tick(float DeltaSeconds) override;
	virtual void CallHandleVisibility(bool Visible) override;
	virtual void CallHandleMateriality(bool Material) override;

private:
	UPROPERTY(Replicated)
	bool netVisible;
	UPROPERTY(Replicated)
	bool netEnabled;
	bool locEnabled;
	bool locVisible;
};

/*
 * ADTPawn
 * The DAT pawn class
 * Part of the DAT API for UE4
 */
UCLASS(AutoExpandCategories=("DAT|DTPawn"))
class DAT_API ADTPawn : public APawn, public IDATActor
{
  GENERATED_BODY()

public:
  ADTPawn(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	/**
	 * Sets the actor's position in the DAT cluster
	 * @param Position - The position to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTPawn")
	void DT_SetLocation(FVector Position);

	/**
	 * Sets the actor's extents
	 * Use this, DT_SetBoundingSphere, or DT_SetBoundingCapsule, but only one as they override each other
	 * @param BoundingBox - The bounding box to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTPawn")
	void DT_SetBoundingBox(FBox BoundingBox);

	/**
	 * Sets the actor's radius
	 * Use this, DT_SetBoundingBox, or DT_SetBoundingCapsule, but only one as they override each other
	 * @param Radius - The radius to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTPawn")
	void DT_SetBoundingSphere(float Radius);

	/**
	 * Sets the actor's radius
	 * Use this, DT_SetBoundingBox, or DT_SetBoundingSphere, but only one as they override each other
	 * @param Radius - The radius to use
	 * @param HalfHeight - The half-height to use
	 * @param CenterOffset - The center offset to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTPawn")
	void DT_SetBoundingCapsule(float Radius, float HalfHeight, float CenterOffset);

	/**
	 * Sets the actor's minimum padding
	 * Use this to set a minimum collision buffer around an actor
	 * @param Padding -The padding to use (set to -1.0 to go back to default padding)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTPawn")
	void DT_SetMinimumPadding(float Padding);

	/**
	 * Sets the scalar threshold which determines when objects are significant enough to be visible (replicated)
	 * @param VisibilityScalar - The scalar to use (calculated as radius divided by distance)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetVisibilityScalar(float VisibilityScalar);

	/**
	 * Sets the actor's rotation
	 * @param Rotation - The rotation to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTPawn")
	void DT_SetRotation(FRotator Rotation);

	/**
	 * Sets the actor's linear velocity
	 * @param Delta - The velocity vector to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTPawn")
	void DT_SetLinearVelocity(FVector Delta);

	/**
	 * Sets the actor's angular velocity
	 * @param AngularDelta - The angular velocity vector to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTPawn")
	void DT_SetAngularVelocity(FVector AngularDelta);

	/**
	 * Sets the actor's density (computes mass given general spherical volume)
	 * @param Density - The density to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTPawn")
	void DT_SetDensity(float Density);

	/**
	 * Sets the actor's mass
	 * @param Mass - The mass to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTPawn")
	void DT_SetMass(float Mass);

	/**
	 * Sets the actor's scale
	 * @param Scale - The scale to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTPawn")
	void DT_SetScale(FVector Scale);

	/**
	 * Sets the actor's luminosity (light radius)
	 * Lighting feature must be enabled on the Master node
	 * @param Luminosity - The radius of the sphere of light around the actor
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTPawn")
	void DT_SetLuminosity(float Luminosity);

	/**
	 * Provides the current game mode
	 * @return The current game mode
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn")
	ADTGameMode* DT_GameMode() const;

	/**
	 * Indicates the actor's current position
	 * @return The actor's current position
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn")
	FVector DT_Location() const;

	/**
	 * Indicates the actor's current rotation
	 * @return The actor's current rotation
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn")
	FRotator DT_Rotation() const;

	/**
	 * Indicates the actor's current position including estimation based on linear velocity
	 * @return The actor's current position with estimation
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn")
	FVector DT_LocationWithEstimation() const;

	/**
	 * Indicates the actor's current rotation including estimation based on angular velocity
	 * @return The actor's current rotation with estimation
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn")
	FRotator DT_RotationWithEstimation() const;

	/**
	 * Indicates the actor's current scale
	 * @return The actor's current scale
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn")
	FVector DT_Scale() const;

	/**
	 * Indicates the actor's current velocity vector
	 * @return The actor's current velocity vector
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn")
	FVector DT_LinearVelocity() const;

	/**
	 * Indicates the actor's current angular velocity vector
	 * @return The actor's current angular velocity vector
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn")
	FVector DT_AngularVelocity() const;

	/**
	 * Indicates the actor's mass
	 * @return The actor's mass
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn")
	float DT_Mass() const;

	/**
	 * Indicates the actor's current luminosity (light radius)
	 * @return The radius of the sphere of light around the actor
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn")
	float DT_Luminosity() const;

	/**
	 * Provides the time (in seconds) since this actor was last updated
	 * Applies to remote actors
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn")
	float DT_Elapsed() const;

	/**
	 * Called when the blueprint should handle actor visibility
	 * @param Visible - True indicates visible, false indicates hidden
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTPawn")
	void DT_SetVisibility(bool Visible);

	/**
	 * Called when the blueprint should enable/disable actor's physics/collision
	 * @param Material - True indicates physical, false indicates ephemeral
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTPawn")
	void DT_SetMateriality(bool Material);

	/**
	 * Handle awareness-add updates, if needed
	 * Allows the actor to keep a list of actors it is aware of
	 * @param Actors - The list of new actors in this actor's awareness
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTPawn")
	void DT_HandleAddToAwareness(const TArray<AActor*>& Actors);

	/**
	 * Handle awareness-remove updates, if needed
	 * Allows the actor to keep a list of actors it is aware of
	 * @param Actors - The list of actors no longer in this actor's awareness
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTPawn")
	void DT_HandleRemoveFromAwareness(const TArray<AActor*>& Actors);

  /**
	 * Handle awareness updates, if needed
	 * @param Actors - The complete list of all actors this actor is currently aware of
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTPawn")
	void DT_HandleAwareness(const TArray<AActor*>& Actors);

	/**
	 * Is this actor being represented in the cluster space?
	 * @return True if this actor is in the cluster, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn")
	bool DT_InCluster() const;

	/**
	 * Is this actor running on a gateway?
	 * @return True if this actor is running on a gateway, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn")
	bool DT_GatewayMode() const;

	/**
	 * Is this actor running on a worker?
	 * @return True if this actor is running on a worker, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn")
	bool DT_WorkerMode() const;

	/**
	 * Is this actor running on a standalone instance?
	 * @return True if this actor is running on a standalone instance, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn")
	bool DT_StandaloneMode() const;

	/**
	 * Indicates the cluster ID of the node simulating this actor
	 * @return This node's cluster ID
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn")
	int DT_ClusterID() const;

	/**
	 * Indicates which kind of actor this is (Authority, Remote, Client)
	 * Similar to HasAuthority() but adds the Remote type
	 * Remote means the actor is a local representation of an actor that is running on another node
	 * @param Target - The DTActor to test (hidden in blueprints)
	 * @param OutBranches - Upon return, holds this actor's type (Authority, Remote, Client)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTPawn", Meta = (BlueprintProtected, ExpandEnumAsExecs = "OutBranches", HidePin = "Target", DefaultToSelf = "Target"))
	static void DT_SwitchHasAuthority(const ADTPawn* Target, DTAuthorityEnum& OutBranches);

	/**
	 * Indicates if the actor is the Authority (not a client or remote server)
	 * @param Target - The actor to test
	 * @return True if the target actor has authority, false otherwise
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTPawn", Meta = (BlueprintProtected, HidePin = "Target", DefaultToSelf = "Target"))
	static bool DT_HasAuthority(const ADTPawn* Target);

	/**
	 * Turns on/off the visibility of the actor across the entire cluster
	 * @param Visible - True to show, false to hide
	 * @param AffectLocal - True to show/hide locally, false to only affect remote representations
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTPawn")
	void DT_SetVisible(bool Visible, bool AffectLocal);

	/**
	 * Turns on/off the collision and/or physics of the actor across the entire cluster
	 * Overrides the default state, call this with Default as the option to reset it
	 * @param State - Enabled to make collidable, Disabled to make insubstantial, Default to let DAT handle it
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTPawn")
	void DT_SetMaterial(DTMaterialityEnum State);

	/**
	 * Triggers an event on a (local) actor that will propagate to its remote representations
	 * @param EventID - The ID of the event to trigger
	 * @param Immediate - Send event immediately (don't wait for replicated variables to be synced)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTPawn")
	void DT_TriggerEvent(int EventID, bool Immediate = false);

	/**
	 * Indicates that this actor should have its motion smoothed on the clients
	 * Smoothing depends on DT_LinearVelocity()
	 * DAT client-side smoothing can conflict with some actors that use their own client-side smoothing (i.e. ACharacter)
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "DAT|DTPawn")
	bool bUseClientSmoothing;

	/**
	 * Indicates that this actor should not use physics at all on the clients
	 * DAT motion smoothing toggles physics on and off based on need (when materiality is not overridden)
	 * This option disables physics on the clients entirely but will still generate enable/disable materiality events (for collisions)
	 * Setting this to true may help client-side performance, but the side-effect is over-penetration during collisions
	 * Ignored when bUseClientSmoothing is false
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "DAT|DTPawn", meta = (EditCondition = "bUseClientSmoothing"))
	bool bNoClientPhysics;

	/**
	 * Indicates that this actor should sweep movement on remote servers and clients (on local do it yourself)
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "DAT|DTPawn")
	bool bSweepMovement;

	/**
	 * Indicates if this actor is the Authority (not a client or remote server)
	 * @return True if this target actor has authority, false otherwise
	 */
	bool DT_HasAuthority() const;

	/**
	 * Indicates if this actor is a remote server representation (not a client or authority server)
	 * @return True if this target actor has authority, false otherwise
	 */
	bool DT_IsRemoteServer() const;

	virtual void Tick(float DeltaSeconds) override;
	virtual void CallHandleVisibility(bool Visible) override;
	virtual void CallHandleMateriality(bool Material) override;

private:
	UPROPERTY(Replicated)
	bool netVisible;
	UPROPERTY(Replicated)
	bool netEnabled;
	bool locEnabled;
	bool locVisible;
};

/*
 * ADTCharacter
 * The DAT character class
 * Part of the DAT API for UE4
 */
UCLASS(AutoExpandCategories=("DAT|DTCharacter"))
class DAT_API ADTCharacter : public ACharacter, public IDATActor
{
  GENERATED_BODY()

public:
  ADTCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	/**
	 * Sets the actor's position in the DAT cluster
	 * @param Position - The position to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTCharacter")
	void DT_SetLocation(FVector Position);

	/**
	 * Sets the actor's extents
	 * Use this, DT_SetBoundingSphere, or DT_SetBoundingCapsule, but only one as they override each other
	 * @param BoundingBox - The bounding box to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTCharacter")
	void DT_SetBoundingBox(FBox BoundingBox);

	/**
	 * Sets the actor's radius
	 * Use this, DT_SetBoundingBox, or DT_SetBoundingCapsule, but only one as they override each other
	 * @param Radius - The radius to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTCharacter")
	void DT_SetBoundingSphere(float Radius);

	/**
	 * Sets the actor's radius
	 * Use this, DT_SetBoundingBox, or DT_SetBoundingSphere, but only one as they override each other
	 * @param Radius - The radius to use
	 * @param HalfHeight - The half-height to use
	 * @param CenterOffset - The center offset to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTCharacter")
	void DT_SetBoundingCapsule(float Radius, float HalfHeight, float CenterOffset);

	/**
	 * Sets the actor's minimum padding
	 * Use this to set a minimum collision buffer around an actor
	 * @param Padding -The padding to use (set to -1.0 to go back to default padding)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTCharacter")
	void DT_SetMinimumPadding(float Padding);

	/**
	 * Sets the scalar threshold which determines when objects are significant enough to be visible (replicated)
	 * @param VisibilityScalar - The scalar to use (calculated as radius divided by distance)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetVisibilityScalar(float VisibilityScalar);

	/**
	 * Sets the actor's rotation
	 * @param Rotation - The rotation to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTCharacter")
	void DT_SetRotation(FRotator Rotation);

	/**
	 * Sets the actor's linear velocity
	 * @param Delta - The velocity vector to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTCharacter")
	void DT_SetLinearVelocity(FVector Delta);

	/**
	 * Sets the actor's angular velocity
	 * @param AngularDelta - The angular velocity vector to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTCharacter")
	void DT_SetAngularVelocity(FVector AngularDelta);

	/**
	 * Sets the actor's density (computes mass given general spherical volume)
	 * @param Density - The density to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTCharacter")
	void DT_SetDensity(float Density);

	/**
	 * Sets the actor's mass
	 * @param Mass - The mass to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTCharacter")
	void DT_SetMass(float Mass);

	/**
	 * Sets the actor's scale
	 * @param Scale - The scale to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTCharacter")
	void DT_SetScale(FVector Scale);

	/**
	 * Sets the actor's luminosity (light radius)
	 * Lighting feature must be enabled on the Master node
	 * @param Luminosity - The radius of the sphere of light around the actor
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTCharacter")
	void DT_SetLuminosity(float Luminosity);

	/**
	 * Provides the current game mode
	 * @return The current game mode
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter")
	ADTGameMode* DT_GameMode() const;

	/**
	 * Indicates the actor's current position
	 * @return The actor's current position
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter")
	FVector DT_Location() const;

	/**
	 * Indicates the actor's current rotation
	 * @return The actor's current rotation
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter")
	FRotator DT_Rotation() const;

	/**
	 * Indicates the actor's current position including estimation based on linear velocity
	 * @return The actor's current position with estimation
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter")
	FVector DT_LocationWithEstimation() const;

	/**
	 * Indicates the actor's current rotation including estimation based on angular velocity
	 * @return The actor's current rotation with estimation
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter")
	FRotator DT_RotationWithEstimation() const;

	/**
	 * Indicates the actor's current scale
	 * @return The actor's current scale
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter")
	FVector DT_Scale() const;

	/**
	 * Indicates the actor's current velocity vector
	 * @return The actor's current velocity vector
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter")
	FVector DT_LinearVelocity() const;

	/**
	 * Indicates the actor's current angular velocity vector
	 * @return The actor's current angular velocity vector
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter")
	FVector DT_AngularVelocity() const;

	/**
	 * Indicates the actor's mass
	 * @return The actor's mass
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter")
	float DT_Mass() const;

	/**
	 * Indicates the actor's current luminosity (light radius)
	 * @return The radius of the sphere of light around the actor
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter")
	float DT_Luminosity() const;

	/**
	 * Provides the time (in seconds) since this actor was last updated
	 * Applies to remote actors
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter")
	float DT_Elapsed() const;

	/**
	 * Called when the blueprint should handle actor visibility
	 * @param Visible - True indicates visible, false indicates hidden
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTCharacter")
	void DT_SetVisibility(bool Visible);

	/**
	 * Called when the blueprint should enable/disable actor's physics/collision
	 * @param Material - True indicates physical, false indicates ephemeral
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTCharacter")
	void DT_SetMateriality(bool Material);

	/**
	 * Handle awareness-add updates, if needed
	 * Allows the actor to keep a list of actors it is aware of
	 * @param Actors - The list of new actors in this actor's awareness
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTCharacter")
	void DT_HandleAddToAwareness(const TArray<AActor*>& Actors);

	/**
	 * Handle awareness-remove updates, if needed
	 * Allows the actor to keep a list of actors it is aware of
	 * @param Actors - The list of actors no longer in this actor's awareness
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTCharacter")
	void DT_HandleRemoveFromAwareness(const TArray<AActor*>& Actors);

  /**
	 * Handle awareness updates, if needed
	 * @param Actors - The complete list of all actors this actor is currently aware of
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTCharacter")
	void DT_HandleAwareness(const TArray<AActor*>& Actors);

	/**
	 * Is this actor being represented in the cluster space?
	 * @return True if this actor is in the cluster, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter")
	bool DT_InCluster() const;

	/**
	 * Is this actor running on a gateway?
	 * @return True if this actor is running on a gateway, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter")
	bool DT_GatewayMode() const;

	/**
	 * Is this actor running on a worker?
	 * @return True if this actor is running on a worker, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter")
	bool DT_WorkerMode() const;

	/**
	 * Is this actor running on a standalone instance?
	 * @return True if this actor is running on a standalone instance, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter")
	bool DT_StandaloneMode() const;

	/**
	 * Indicates the cluster ID of the node simulating this actor
	 * @return This node's cluster ID
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter")
	int DT_ClusterID() const;

	/**
	 * Indicates which kind of actor this is (Authority, Remote, Client)
	 * Similar to HasAuthority() but adds the Remote type
	 * Remote means the actor is a local representation of an actor that is running on another node
	 * @param Target - The DTActor to test (hidden in blueprints)
	 * @param OutBranches - Upon return, holds this actor's type (Authority, Remote, Client)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTCharacter", Meta = (BlueprintProtected, ExpandEnumAsExecs = "OutBranches", HidePin = "Target", DefaultToSelf = "Target"))
	static void DT_SwitchHasAuthority(const ADTCharacter* Target, DTAuthorityEnum& OutBranches);

	/**
	 * Indicates if the actor is the Authority (not a client or remote server)
	 * @param Target - The actor to test
	 * @return True if the target actor has authority, false otherwise
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTCharacter", Meta = (BlueprintProtected, HidePin = "Target", DefaultToSelf = "Target"))
	static bool DT_HasAuthority(const ADTCharacter* Target);

	/**
	 * Turns on/off the visibility of the actor across the entire cluster
	 * @param Visible - True to show, false to hide
	 * @param AffectLocal - True to show/hide locally, false to only affect remote representations
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTCharacter")
	void DT_SetVisible(bool Visible, bool AffectLocal);

	/**
	 * Turns on/off the collision and/or physics of the actor across the entire cluster
	 * Overrides the default state, call this with Default as the option to reset it
	 * @param State - Enabled to make collidable, Disabled to make insubstantial, Default to let DAT handle it
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTCharacter")
	void DT_SetMaterial(DTMaterialityEnum State);

	/**
	 * Triggers an event on a (local) actor that will propagate to its remote representations
	 * @param EventID - The ID of the event to trigger
	 * @param Immediate - Send event immediately (don't wait for replicated variables to be synced)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTCharacter")
	void DT_TriggerEvent(int EventID, bool Immediate = false);

	/**
	 * Indicates that this actor should have its motion smoothed on the clients
	 * Smoothing depends on DT_LinearVelocity()
	 * DAT client-side smoothing can conflict with some actors that use their own client-side smoothing (i.e. ACharacter)
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "DAT|DTCharacter")
	bool bUseClientSmoothing;

	/**
	 * Indicates that this actor should not use physics at all on the clients
	 * DAT motion smoothing toggles physics on and off based on need (when materiality is not overridden)
	 * This option disables physics on the clients entirely but will still generate enable/disable materiality events (for collisions)
	 * Setting this to true may help client-side performance, but the side-effect is over-penetration during collisions
	 * Ignored when bUseClientSmoothing is false
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "DAT|DTCharacter", meta = (EditCondition = "bUseClientSmoothing"))
	bool bNoClientPhysics;

	/**
	 * Indicates that this actor should sweep movement on remote servers and clients (on local do it yourself)
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "DAT|DTCharacter")
	bool bSweepMovement;

	/**
	 * Indicates if this actor is the Authority (not a client or remote server)
	 * @return True if this target actor has authority, false otherwise
	 */
	bool DT_HasAuthority() const;

	/**
	 * Indicates if this actor is a remote server representation (not a client or authority server)
	 * @return True if this target actor has authority, false otherwise
	 */
	bool DT_IsRemoteServer() const;

	virtual void Tick(float DeltaSeconds) override;
	virtual void CallHandleVisibility(bool Visible) override;
	virtual void CallHandleMateriality(bool Material) override;

private:
	UPROPERTY(Replicated)
	bool netVisible;
	UPROPERTY(Replicated)
	bool netEnabled;
	bool locEnabled;
	bool locVisible;
};

/*
 * ADTWheeledVehicle
 * The DAT wheeled vehicle class
 * Part of the DAT API for UE4
 */
UCLASS(AutoExpandCategories=("DAT|DTWheeledVehicle"))
class DAT_API ADTWheeledVehicle : public AWheeledVehicle, public IDATActor
{
  GENERATED_BODY()

public:
  ADTWheeledVehicle(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	/**
	 * Sets the actor's position in the DAT cluster
	 * @param Position - The position to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTWheeledVehicle")
	void DT_SetLocation(FVector Position);

	/**
	 * Sets the actor's extents
	 * Use this, DT_SetBoundingSphere, or DT_SetBoundingCapsule, but only one as they override each other
	 * @param BoundingBox - The bounding box to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTWheeledVehicle")
	void DT_SetBoundingBox(FBox BoundingBox);

	/**
	 * Sets the actor's radius
	 * Use this, DT_SetBoundingBox, or DT_SetBoundingCapsule, but only one as they override each other
	 * @param Radius - The radius to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTWheeledVehicle")
	void DT_SetBoundingSphere(float Radius);

	/**
	 * Sets the actor's radius
	 * Use this, DT_SetBoundingBox, or DT_SetBoundingSphere, but only one as they override each other
	 * @param Radius - The radius to use
	 * @param HalfHeight - The half-height to use
	 * @param CenterOffset - The center offset to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTWheeledVehicle")
	void DT_SetBoundingCapsule(float Radius, float HalfHeight, float CenterOffset);

	/**
	 * Sets the actor's minimum padding
	 * Use this to set a minimum collision buffer around an actor
	 * @param Padding -The padding to use (set to -1.0 to go back to default padding)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTWheeledVehicle")
	void DT_SetMinimumPadding(float Padding);

	/**
	 * Sets the scalar threshold which determines when objects are significant enough to be visible (replicated)
	 * @param VisibilityScalar - The scalar to use (calculated as radius divided by distance)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTActor")
	void DT_SetVisibilityScalar(float VisibilityScalar);

	/**
	 * Sets the actor's rotation
	 * @param Rotation - The rotation to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTWheeledVehicle")
	void DT_SetRotation(FRotator Rotation);

	/**
	 * Sets the actor's linear velocity
	 * @param Delta - The velocity vector to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTWheeledVehicle")
	void DT_SetLinearVelocity(FVector Delta);

	/**
	 * Sets the actor's angular velocity
	 * @param AngularDelta - The angular velocity vector to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTWheeledVehicle")
	void DT_SetAngularVelocity(FVector AngularDelta);

	/**
	 * Sets the actor's density (computes mass given general spherical volume)
	 * @param Density - The density to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTWheeledVehicle")
	void DT_SetDensity(float Density);

	/**
	 * Sets the actor's mass
	 * @param Mass - The mass to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTWheeledVehicle")
	void DT_SetMass(float Mass);

	/**
	 * Sets the actor's scale
	 * @param Scale - The scale to use
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTWheeledVehicle")
	void DT_SetScale(FVector Scale);

	/**
	 * Sets the actor's luminosity (light radius)
	 * Lighting feature must be enabled on the Master node
	 * @param Luminosity - The radius of the sphere of light around the actor
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTWheeledVehicle")
	void DT_SetLuminosity(float Luminosity);

	/**
	 * Provides the current game mode
	 * @return The current game mode
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle")
	ADTGameMode* DT_GameMode() const;

	/**
	 * Indicates the actor's current position
	 * @return The actor's current position
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle")
	FVector DT_Location() const;

	/**
	 * Indicates the actor's current rotation
	 * @return The actor's current rotation
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle")
	FRotator DT_Rotation() const;

	/**
	 * Indicates the actor's current position including estimation based on linear velocity
	 * @return The actor's current position with estimation
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle")
	FVector DT_LocationWithEstimation() const;

	/**
	 * Indicates the actor's current rotation including estimation based on angular velocity
	 * @return The actor's current rotation with estimation
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle")
	FRotator DT_RotationWithEstimation() const;

	/**
	 * Indicates the actor's current scale
	 * @return The actor's current scale
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle")
	FVector DT_Scale() const;

	/**
	 * Indicates the actor's current velocity vector
	 * @return The actor's current velocity vector
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle")
	FVector DT_LinearVelocity() const;

	/**
	 * Indicates the actor's current angular velocity vector
	 * @return The actor's current angular velocity vector
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle")
	FVector DT_AngularVelocity() const;

	/**
	 * Indicates the actor's mass
	 * @return The actor's mass
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle")
	float DT_Mass() const;

	/**
	 * Indicates the actor's current luminosity (light radius)
	 * @return The radius of the sphere of light around the actor
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle")
	float DT_Luminosity() const;

	/**
	 * Provides the time (in seconds) since this actor was last updated
	 * Applies to remote actors
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle")
	float DT_Elapsed() const;

	/**
	 * Called when the blueprint should handle actor visibility
	 * @param Visible - True indicates visible, false indicates hidden
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTWheeledVehicle")
	void DT_SetVisibility(bool Visible);

	/**
	 * Called when the blueprint should enable/disable actor's physics/collision
	 * @param Material - True indicates physical, false indicates ephemeral
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTWheeledVehicle")
	void DT_SetMateriality(bool Material);

	/**
	 * Handle awareness-add updates, if needed
	 * Allows the actor to keep a list of actors it is aware of
	 * @param Actors - The list of new actors in this actor's awareness
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTWheeledVehicle")
	void DT_HandleAddToAwareness(const TArray<AActor*>& Actors);

	/**
	 * Handle awareness-remove updates, if needed
	 * Allows the actor to keep a list of actors it is aware of
	 * @param Actors - The list of actors no longer in this actor's awareness
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTWheeledVehicle")
	void DT_HandleRemoveFromAwareness(const TArray<AActor*>& Actors);

  /**
	 * Handle awareness updates, if needed
	 * @param Actors - The complete list of all actors this actor is currently aware of
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "DAT|DTWheeledVehicle")
	void DT_HandleAwareness(const TArray<AActor*>& Actors);

	/**
	 * Is this actor being represented in the cluster space?
	 * @return True if this actor is in the cluster, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle")
	bool DT_InCluster() const;

	/**
	 * Is this actor running on a gateway?
	 * @return True if this actor is running on a gateway, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle")
	bool DT_GatewayMode() const;

	/**
	 * Is this actor running on a worker?
	 * @return True if this actor is running on a worker, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle")
	bool DT_WorkerMode() const;

	/**
	 * Is this actor running on a standalone instance?
	 * @return True if this actor is running on a standalone instance, false if not
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle")
	bool DT_StandaloneMode() const;

	/**
	 * Indicates the cluster ID of the node simulating this actor
	 * @return This node's cluster ID
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle")
	int DT_ClusterID() const;

	/**
	 * Indicates which kind of actor this is (Authority, Remote, Client)
	 * Similar to HasAuthority() but adds the Remote type
	 * Remote means the actor is a local representation of an actor that is running on another node
	 * @param Target - The DTActor to test (hidden in blueprints)
	 * @param OutBranches - Upon return, holds this actor's type (Authority, Remote, Client)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTWheeledVehicle", Meta = (BlueprintProtected, ExpandEnumAsExecs = "OutBranches", HidePin = "Target", DefaultToSelf = "Target"))
	static void DT_SwitchHasAuthority(const ADTWheeledVehicle* Target, DTAuthorityEnum& OutBranches);

	/**
	 * Indicates if the actor is the Authority (not a client or remote server)
	 * @param Target - The actor to test
	 * @return True if the target actor has authority, false otherwise
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DAT|DTWheeledVehicle", Meta = (BlueprintProtected, HidePin = "Target", DefaultToSelf = "Target"))
	static bool DT_HasAuthority(const ADTWheeledVehicle* Target);

	/**
	 * Turns on/off the visibility of the actor across the entire cluster
	 * @param Visible - True to show, false to hide
	 * @param AffectLocal - True to show/hide locally, false to only affect remote representations
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTWheeledVehicle")
	void DT_SetVisible(bool Visible, bool AffectLocal);

	/**
	 * Turns on/off the collision and/or physics of the actor across the entire cluster
	 * Overrides the default state, call this with Default as the option to reset it
	 * @param State - Enabled to make collidable, Disabled to make insubstantial, Default to let DAT handle it
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTWheeledVehicle")
	void DT_SetMaterial(DTMaterialityEnum State);

	/**
	 * Triggers an event on a (local) actor that will propagate to its remote representations
	 * @param EventID - The ID of the event to trigger
	 * @param Immediate - Send event immediately (don't wait for replicated variables to be synced)
	 */
	UFUNCTION(BlueprintCallable, Category = "DAT|DTWheeledVehicle")
	void DT_TriggerEvent(int EventID, bool Immediate = false);

	/**
	 * Indicates that this actor should have its motion smoothed on the clients
	 * Smoothing depends on DT_LinearVelocity()
	 * DAT client-side smoothing can conflict with some actors that use their own client-side smoothing (i.e. ACharacter)
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "DAT|DTWheeledVehicle")
	bool bUseClientSmoothing;

	/**
	 * Indicates that this actor should not use physics at all on the clients
	 * DAT motion smoothing toggles physics on and off based on need (when materiality is not overridden)
	 * This option disables physics on the clients entirely but will still generate enable/disable materiality events (for collisions)
	 * Setting this to true may help client-side performance, but the side-effect is over-penetration during collisions
	 * Ignored when bUseClientSmoothing is false
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "DAT|DTWheeledVehicle", meta = (EditCondition = "bUseClientSmoothing"))
	bool bNoClientPhysics;

	/**
	 * Indicates that this actor should sweep movement on remote servers and clients (on local do it yourself)
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "DAT|DTWheeledVehicle")
	bool bSweepMovement;

	/**
	 * Indicates if this actor is the Authority (not a client or remote server)
	 * @return True if this target actor has authority, false otherwise
	 */
	bool DT_HasAuthority() const;

	/**
	 * Indicates if this actor is a remote server representation (not a client or authority server)
	 * @return True if this target actor has authority, false otherwise
	 */
	bool DT_IsRemoteServer() const;

	virtual void Tick(float DeltaSeconds) override;
	virtual void CallHandleVisibility(bool Visible) override;
	virtual void CallHandleMateriality(bool Material) override;

private:
	UPROPERTY(Replicated)
	bool netVisible;
	UPROPERTY(Replicated)
	bool netEnabled;
	bool locEnabled;
	bool locVisible;
};

class DAT_API DTType
{
public:
	DTType(ADTGameMode* gm, char const* nameString, int type, int maxLocalCnt, UClass* remActorClass, DTBehavior behavior);
};

template<class ENTITY, class ACTOR>
class DTType_Impl : public DTType
{
public:
	DTType_Impl(ADTGameMode* gm, char const* name, int type, int maxLocalCnt, UClass* actorClass, DTBehavior behavior)
		: DTType(gm, name, type, maxLocalCnt, actorClass, behavior)
	{
	}
};
