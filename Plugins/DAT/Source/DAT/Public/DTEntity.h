/****************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations.  The intellectual and technical
 * concepts contained herein are proprietary to Nocturnal Innovations
 * and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the authorized form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API in combination with one or more example
 * projects. All other uses are strictly forbidden unless prior written
 * permission is obtained from Nocturnal Innovations.
 */

#pragma once

#include "DTDefs.h"
#include "GameFramework/Actor.h"

enum class DTAuthorityEnum : uint8;

class DAT_API DTEntity
{
public:
	DTEntity();
	virtual ~DTEntity();

	virtual void Load();
	virtual void Store();

	void DT_ReadData(byte* data, int len);
	void DT_WriteData(const byte* data, int len);
	void DT_ReadString(FString& String);
	int DT_StringLen(const FString& String) const;
	void DT_WriteString(const FString& String);

	template <class T>
	void DT_ReadObject(T& obj)
	{
	}


	template <class T>
	void DT_WriteObject(const T& obj)
	{
	}

public:
	void* dtt;
};

