/****************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations.  The intellectual and technical
 * concepts contained herein are proprietary to Nocturnal Innovations
 * and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the authorized form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API in combination with one or more example
 * projects. All other uses are strictly forbidden unless prior written
 * permission is obtained from Nocturnal Innovations.
 */

#include "DTEntity.h"
#include "DTActor.h"
#include "Components/PrimitiveComponent.h"

DTEntity::DTEntity()
{
}

DTEntity::~DTEntity()
{
}

void DTEntity::Load()
{
}

void DTEntity::Store()
{
}

void DTEntity::DT_ReadData(byte* data, int len)
{
}

void DTEntity::DT_WriteData(const byte* data, int len)
{
}

void DTEntity::DT_ReadString(FString& String)
{
}

int DTEntity::DT_StringLen(const FString& String) const
{
	return 0;
}

void DTEntity::DT_WriteString(const FString& String)
{
}