/****************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations.  The intellectual and technical
 * concepts contained herein are proprietary to Nocturnal Innovations
 * and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the authorized form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API in combination with one or more example
 * projects. All other uses are strictly forbidden unless prior written
 * permission is obtained from Nocturnal Innovations.
 */

#include "DATActor.h"
#include "DTActor.h"
#include "DTGameMode.h"
#include "PhysicsEngine/BodyInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Components/PrimitiveComponent.h"
#include "Classes/GameFramework/Actor.h"
#include "Classes/GameFramework/Pawn.h"
#include "Engine/World.h"

UDATActor::UDATActor(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}


IDATActor::IDATActor()
{
	outData = NULL;
	outSize = 0;
	outCur = 0;
	inData = NULL;
	inSize = 0;
	inCur = 0;
}

void IDATActor::DA_SetLocation(FVector xyz)
{
}

void IDATActor::DA_SetBoundingBox(FBox Extents)
{
}

void IDATActor::DA_SetBoundingSphere(float r)
{
}

void IDATActor::DA_SetBoundingCapsule(float Radius, float HalfHeight, float CenterHeight)
{
}

void IDATActor::DA_SetMinimumPadding(float p)
{
}

void IDATActor::DA_SetRotation(FRotator rotation)
{
}

void IDATActor::DA_SetLinearVelocity(FVector delta)
{
}

void IDATActor::DA_SetAngularVelocity(FVector angularDelta)
{
}

void IDATActor::DA_SetDensity(float density)
{
}

void IDATActor::DA_SetMass(float mass)
{
}

void IDATActor::DA_SetScale(FVector scale)
{
}

void IDATActor::DA_SetLuminosity(float luminosity)
{
}

ADTGameMode* IDATActor::DA_GameMode() const
{
	return Cast<ADTGameMode>(Cast<AActor>(this)->GetWorld()->GetAuthGameMode());
}

FVector IDATActor::DA_Location() const
{
	return FVector::ZeroVector;
}

FRotator IDATActor::DA_Rotation() const
{
	return FRotator::ZeroRotator;
}

FVector IDATActor::DA_LocationWithEstimation() const
{
	return FVector::ZeroVector;
}

FRotator IDATActor::DA_RotationWithEstimation() const
{
	return FRotator::ZeroRotator;
}

FVector IDATActor::DA_Scale() const
{
	return FVector::ZeroVector;
}

FVector IDATActor::DA_LinearVelocity() const
{
	return FVector::ZeroVector;
}

FVector IDATActor::DA_AngularVelocity() const
{
	return FVector::ZeroVector;
}

float IDATActor::DA_Mass() const
{
	return 0.0f;
}

float IDATActor::DA_Luminosity() const
{
	return 0.0f;
}

float IDATActor::DA_Elapsed() const
{
	return 0.0f;
}

void IDATActor::DT_EnableAwarenessUpdates()
{
}

void IDATActor::DT_GetAwarenessState(FVector& pos, float& scalar)
{
}

bool IDATActor::DA_IsVisible() const
{
	return true;
}

bool IDATActor::DA_IsEnabled() const
{
	return true;
}

bool IDATActor::DA_HasEntity() const
{
	return false;
}

bool IDATActor::DA_GatewayMode() const
{
	return false;
}

bool IDATActor::DA_WorkerMode() const
{
	return false;
}

bool IDATActor::DA_StandaloneMode() const
{
	return Cast<AActor>(this)->HasAuthority();
}

int IDATActor::DA_ClusterID() const
{
	return -1;
}

void IDATActor::DA_SwitchHasAuthority(const IDATActor* Target, DTAuthorityEnum& Branches)
{
	const AActor* a = Cast<const AActor>(Target);
	if(a->HasAuthority())
		Branches = DTAuthorityEnum::Authority;
	else
		Branches = DTAuthorityEnum::Client;
}

bool IDATActor::DA_HasAuthority(const IDATActor* Target)
{
	return Cast<const AActor>(Target)->HasAuthority();
}

bool IDATActor::DA_HasAuthority() const
{
	const AActor* act = Cast<AActor>(this);
	check(act != NULL);
	return act->HasAuthority();
}

DTEntity* IDATActor::DT_Entity()
{
	return NULL;
}

void IDATActor::DT_PrepareEvent(int MessageLength)
{
	DT_PrepareMessage(MessageLength);
}

void IDATActor::DT_PrepareMessage(int MessageLength)
{
	if(outData != NULL)
	{
		if(MessageLength == outSize)
		{
			outCur = 0;
			return;
		}

		delete outData;
	}

	outSize = MessageLength;
	if(outSize > 0)
	{
		outData = new byte[outSize];
		outCur = 0;
	}
	else
		outData = NULL;
}

void IDATActor::DT_ReadData(byte* data, int len)
{
	memcpy(data, inData + inCur, len);
	inCur += len;
}

void IDATActor::DT_WriteData(const byte* data, int len)
{
	memcpy(outData + outCur, data, len);
	outCur += len;
}

void IDATActor::DT_ReadString(FString& String)
{
	int strLen;

	DT_ReadObject<int>(strLen);

	char* str = new char[strLen + 1];
	DT_ReadData((byte*) str, strLen);
	str[strLen] = '\0';

	String = str;
	delete str;
}

int IDATActor::DT_StringLen(const FString& String) const
{
	return sizeof(int) + String.Len();
}

void IDATActor::DT_WriteString(const FString& String)
{
	char* stringStr = TCHAR_TO_ANSI(*String);
	int stringStrLen = String.Len();

	DT_WriteObject<int>(stringStrLen);
	DT_WriteData((byte*) stringStr, stringStrLen);
}

void IDATActor::DT_SendMessage(AActor* to, int messageType)
{
	IDATActor* a = Cast<IDATActor>(to);
	if(a != NULL)
		a->HandleMessage(Cast<AActor>(this), messageType, outData, outCur);

	outCur = 0;
}

void IDATActor::DT_SendGroupMessage(const TArray<AActor*>& toList, int messageType)
{
	for(int i = 0; i < toList.Num(); i++)
	{
		AActor* to = toList[i];
		IDATActor* a = Cast<IDATActor>(to);
		if(a != NULL)
			a->HandleMessage(Cast<AActor>(this), messageType, outData, outCur);
	}

	outCur = 0;
}

void IDATActor::DT_EmitMessage(FVector Location, float Radius, int messageType)
{
	TArray<AActor*> toList;

	AActor* thisActor = Cast<AActor>(this);
	TArray<TEnumAsByte<EObjectTypeQuery> > dummy;
	UKismetSystemLibrary::SphereOverlapActors(thisActor, thisActor->GetActorLocation(), Radius, dummy, NULL, TArray<AActor*>(), toList);

	for(int i = 0; i < toList.Num(); i++)
	{
		DTEntity* toEnt = NULL;
		AActor* to = toList[i];
		IDATActor* a = Cast<IDATActor>(to);
		if(a != NULL)
			a->HandleMessage(thisActor, messageType, outData, outCur);
	}

	outCur = 0;
}

void IDATActor::HandleEvent(int eventType, byte* data, int dataSz)
{
	inData = data;
	inSize = dataSz;
	inCur = 0;
	DT_HandleEvent(eventType);
	inData = NULL;
	inSize = 0;
}

void IDATActor::HandleMessage(AActor* from, int messageType, byte* data, int dataSz)
{
	inData = data;
	inSize = dataSz;
	inCur = 0;
	DT_HandleMessage(from, messageType);
	inData = NULL;
	inSize = 0;
}

void IDATActor::DT_HandleMessage(AActor* from, int messageType)
{
}

int IDATActor::DT_GetType() const
{
	return -1;
}

UActorComponent* IDATActor::DT_GetComponent(int ID) const
{
	return NULL;
}

int IDATActor::DT_GetComponentID(const UActorComponent* Component) const
{
	return 0;
}

EntityAddr IDATActor::DT_Address() const
{
	return EntityAddr::InvalidAddr();
}

void IDATActor::DT_Show()
{
	switch(baseType)
	{
	case 0:
	{
		ADTActor* x = Cast<ADTActor>(this);
		check(x != NULL);
		x->DT_SetVisibility(true);
		break;
	}
	case 1:
	{
		ADTCharacter* x = Cast<ADTCharacter>(this);
		check(x != NULL);
		x->DT_SetVisibility(true);
		break;
	}
	case 2:
	{
		ADTPawn* x = Cast<ADTPawn>(this);
		check(x != NULL);
		x->DT_SetVisibility(true);
		break;
	}
	case 3:
	{
		ADTWheeledVehicle* x = Cast<ADTWheeledVehicle>(this);
		check(x != NULL);
		x->DT_SetVisibility(true);
		break;
	}
	default:
		check(false);
	}
}

void IDATActor::DT_Hide()
{
	switch(baseType)
	{
	case 0:
	{
		ADTActor* x = Cast<ADTActor>(this);
		check(x != NULL);
		x->DT_SetVisibility(false);
		break;
	}
	case 1:
	{
		ADTCharacter* x = Cast<ADTCharacter>(this);
		check(x != NULL);
		x->DT_SetVisibility(false);
		break;
	}
	case 2:
	{
		ADTPawn* x = Cast<ADTPawn>(this);
		check(x != NULL);
		x->DT_SetVisibility(false);
		break;
	}
	case 3:
	{
		ADTWheeledVehicle* x = Cast<ADTWheeledVehicle>(this);
		check(x != NULL);
		x->DT_SetVisibility(false);
		break;
	}
	default:
		check(false);
	}
}

void IDATActor::DT_Enable()
{
	switch(baseType)
	{
	case 0:
	{
		ADTActor* x = Cast<ADTActor>(this);
		check(x != NULL);
		x->DT_SetMateriality(true);
		break;
	}
	case 1:
	{
		ADTCharacter* x = Cast<ADTCharacter>(this);
		check(x != NULL);
		x->DT_SetMateriality(true);
		break;
	}
	case 2:
	{
		ADTPawn* x = Cast<ADTPawn>(this);
		check(x != NULL);
		x->DT_SetMateriality(true);
		break;
	}
	case 3:
	{
		ADTWheeledVehicle* x = Cast<ADTWheeledVehicle>(this);
		check(x != NULL);
		x->DT_SetMateriality(true);
		break;
	}
	default:
		check(false);
	}
}

void IDATActor::DT_Disable()
{
	switch(baseType)
	{
	case 0:
	{
		ADTActor* x = Cast<ADTActor>(this);
		check(x != NULL);
		x->DT_SetMateriality(false);
		break;
	}
	case 1:
	{
		ADTCharacter* x = Cast<ADTCharacter>(this);
		check(x != NULL);
		x->DT_SetMateriality(false);
		break;
	}
	case 2:
	{
		ADTPawn* x = Cast<ADTPawn>(this);
		check(x != NULL);
		x->DT_SetMateriality(false);
		break;
	}
	case 3:
	{
		ADTWheeledVehicle* x = Cast<ADTWheeledVehicle>(this);
		check(x != NULL);
		x->DT_SetMateriality(false);
		break;
	}
	default:
		check(false);
	}
}

void IDATActor::DA_SetVisibilityScalar(float VisibilityScalar)
{
}

void IDATActor::DA_SetVisible(bool Visible, bool AffectLocal)
{
#ifndef DAT_CLIENT_MODE
	if(Visible)
		DT_Show();
	else
		DT_Hide();

	CallHandleVisibility(Visible);
#endif
}

void IDATActor::DA_SetMaterial(DTMaterialityEnum State)
{
#ifndef DAT_CLIENT_MODE
	if(State == DTMaterialityEnum::Disabled)
	{
		DT_Disable();
		CallHandleMateriality(false);
	}
	else
	{
		DT_Enable();
		CallHandleMateriality(true);
	}
#endif
}

void IDATActor::DT_Initialize()
{
}

void IDATActor::DT_Activate()
{
}

void IDATActor::DT_Destroy()
{
}

double IDATActor::SCTime() const
{
	return FPlatformTime::Seconds() - ADTGameMode::StartupTime;
}

double IDATActor::CCTime() const
{
	return FPlatformTime::Seconds() - ADTGameMode::StartupTime;
}

void IDATActor::DT_Duplicate()
{
}

void IDATActor::DT_Reflect(bool ClusterUpdate)
{
}

void IDATActor::DT_Update()
{
}

void IDATActor::DA_TriggerEvent(int eventID, bool immediate)
{
}

double IDATActor::DT_Stamp() const
{
	return 0.0;
}

double IDATActor::DT_ClusterTime() const
{
	return 0.0;
}

bool IDATActor::DA_InCluster() const
{
	return false;
}

void IDATActor::DT_SetClusterLinearTolerance(float Tolerance)
{
}

void IDATActor::DT_SetClusterAngularTolerance(float Tolerance)
{
}

void IDATActor::DT_HandleEvent(int eventID)
{
}

void IDATActor::DT_HandleGravity(FVector grav)
{
}

void IDATActor::CallHandleVisibility(bool Visible)
{
#ifndef DAT_CLIENT_MODE
	check(false);
#endif
}

void IDATActor::CallHandleMateriality(bool Material)
{
#ifndef DAT_CLIENT_MODE
	check(false);
#endif
}

