/****************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations.  The intellectual and technical
 * concepts contained herein are proprietary to Nocturnal Innovations
 * and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the authorized form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API in combination with one or more example
 * projects. All other uses are strictly forbidden unless prior written
 * permission is obtained from Nocturnal Innovations.
 */

#include "DTActor.h"
#include "Kismet/GameplayStatics.h"
#include "Components/PrimitiveComponent.h"


#define DAIFUNCTION0(y,x) y::y(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) { baseType = x; bSweepMovement = false; bUseClientSmoothing = false; locEnabled = netEnabled = true; locVisible = netVisible = true; }
#define DAIFUNCTION2(x,y,z) x y::DT_##z() const { return DA_##z(); }
#define DAIFUNCTION7(y,z,a) void y::DT_##z(a p) { DA_##z(p); }
#define DAIFUNCTION9(y,z,a,b) void y::DT_##z(a p1, b p2) { DA_##z(p1, p2); }
#define DAIFUNCTION10(y,z,a,b,c) void y::DT_##z(a p1, b p2, c p3) { DA_##z(p1, p2, p3); }
#define DAIFUNCTION12(y,z,a) void y::z(a p1) { Super::z(p1); DA##z(p1); }
#define DAIFUNCTION14(y,z,a,b) void y::DT_##z(a p1, b p2) { DA_##z((IDATActor*) p1, p2); }
#define DAIFUNCTION16(x,y,z,a) x y::DT_##z(a p1) { return DA_##z(p1); }
#define DAIFUNCTION17(y,z) void y::z() { Super::z(); DA##z(); }
#define DAIFUNCTION18(y,z,a) void y::z(a p1) { if(DA##z(p1)) Super::z(p1); }
#define DAIFUNCTION19(y,z) void y::CallHandle##z(bool b) { if(b != netVisible) netVisible = b; }
#define DAIFUNCTION23(x,y,z) x y::DT_##z() const { x p = DA_##z(); return p; }
#define DAIFUNCTION25(y,z,a) void y::DT_Set##z(a p) { DA_Set##z(p); }
#define DAIFUNCTION26(y,z) void y::CallHandle##z(bool b) { if((GetRemoteRole() != ROLE_AutonomousProxy) && (b != netEnabled)) netEnabled = b; }

#define DEFINE_DAT_API_CLASS(ClassName,ClassID)\
DAIFUNCTION0(ClassName,ClassID)\
DAIFUNCTION2(FVector,ClassName,Location)\
DAIFUNCTION2(FRotator,ClassName,Rotation)\
DAIFUNCTION2(FVector,ClassName,LocationWithEstimation)\
DAIFUNCTION2(FRotator,ClassName,RotationWithEstimation)\
DAIFUNCTION2(FVector,ClassName,Scale)\
DAIFUNCTION23(FVector,ClassName,LinearVelocity)\
DAIFUNCTION23(FVector,ClassName,AngularVelocity)\
DAIFUNCTION2(float,ClassName,Mass)\
DAIFUNCTION2(float,ClassName,Luminosity)\
DAIFUNCTION2(float,ClassName,Elapsed)\
DAIFUNCTION2(bool,ClassName,InCluster)\
DAIFUNCTION2(bool,ClassName,GatewayMode)\
DAIFUNCTION2(bool,ClassName,WorkerMode)\
DAIFUNCTION2(bool,ClassName,StandaloneMode)\
DAIFUNCTION2(int,ClassName,ClusterID)\
DAIFUNCTION2(ADTGameMode*,ClassName,GameMode)\
DAIFUNCTION7(ClassName,SetLocation,FVector)\
DAIFUNCTION7(ClassName,SetBoundingBox,FBox)\
DAIFUNCTION7(ClassName,SetBoundingSphere,float)\
DAIFUNCTION10(ClassName,SetBoundingCapsule,float,float,float)\
DAIFUNCTION7(ClassName,SetMinimumPadding,float)\
DAIFUNCTION7(ClassName,SetVisibilityScalar,float)\
DAIFUNCTION7(ClassName,SetRotation,FRotator)\
DAIFUNCTION7(ClassName,SetDensity,float)\
DAIFUNCTION7(ClassName,SetMass,float)\
DAIFUNCTION7(ClassName,SetScale,FVector)\
DAIFUNCTION7(ClassName,SetLuminosity,float)\
DAIFUNCTION9(ClassName,TriggerEvent,int,bool)\
DAIFUNCTION25(ClassName,LinearVelocity,FVector)\
DAIFUNCTION25(ClassName,AngularVelocity,FVector)\
DAIFUNCTION14(ClassName,SwitchHasAuthority,const ClassName*,DTAuthorityEnum&)\
DAIFUNCTION16(bool,ClassName,HasAuthority,const ClassName*)\
DAIFUNCTION2(bool,ClassName,HasAuthority)\
DAIFUNCTION9(ClassName,SetVisible,bool,bool)\
DAIFUNCTION7(ClassName,SetMaterial,DTMaterialityEnum)\
DAIFUNCTION19(ClassName,Visibility)\
DAIFUNCTION26(ClassName,Materiality)\
void ClassName::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const\
{\
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);\
	DOREPLIFETIME(ClassName, netVisible);\
	DOREPLIFETIME(ClassName, netEnabled);\
}\
void ClassName::Tick(float DeltaSeconds)\
{\
	if(Role != ROLE_Authority)\
	{\
		if((Role != ROLE_AutonomousProxy) && (netEnabled != locEnabled) && netEnabled)\
		{\
			locEnabled = true;\
			DT_Enable();\
		}\
		Super::Tick(DeltaSeconds);\
		if((Role != ROLE_AutonomousProxy) && (netEnabled != locEnabled) && !netEnabled)\
		{\
			locEnabled = false;\
			DT_Disable();\
		}\
		if(netVisible != locVisible)\
		{\
			locVisible = netVisible;\
			if(locVisible)\
				DT_Show();\
			else\
				DT_Hide();\
		}\
	}\
	else\
		Super::Tick(DeltaSeconds);\
}

DEFINE_DAT_API_CLASS(ADTActor, 0)
DEFINE_DAT_API_CLASS(ADTCharacter, 1)
DEFINE_DAT_API_CLASS(ADTPawn, 2)
DEFINE_DAT_API_CLASS(ADTWheeledVehicle, 3)

DTType::DTType(ADTGameMode* gm, char const* nameString, int type, int maxLocalCnt, UClass* remActorClass, DTBehavior behavior)
{
}
