/****************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations.  The intellectual and technical
 * concepts contained herein are proprietary to Nocturnal Innovations
 * and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the authorized form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API in combination with one or more example
 * projects. All other uses are strictly forbidden unless prior written
 * permission is obtained from Nocturnal Innovations.
 */

#include "DTGameMode.h"
#include "DAT.h"
#include "DTActor.h"
#include "EngineUtils.h"
#include "Classes/Engine/Engine.h"

double ADTGameMode::StartupTime = 0.0;

ADTGameMode::ADTGameMode()
{
	outData = NULL;
	outSize = 0;
	outCur = 0;
	inData = NULL;
	inSize = 0;
	inCur = 0;
}

void ADTGameMode::DT_ConnectResult(DTConnResult result)
{
}

void ADTGameMode::DT_Initialize()
{
}

void ADTGameMode::DT_UpdateEntities()
{
}

bool ADTGameMode::DT_MasterMode()
{
	return false;
}

bool ADTGameMode::DT_GatewayMode()
{
	return false;
}

bool ADTGameMode::DT_WorkerMode()
{
	return false;
}

bool ADTGameMode::DT_StandaloneMode()
{
	return true;
}

void ADTGameMode::DT_HandleMasterMessage(int from, int type)
{
}

float ADTGameMode::DT_UpdatesPerSecond()
{
	return 0.0f;
}

float ADTGameMode::DT_SecondsPerUpdate()
{
	return 0.0f;
}

int32 ADTGameMode::DT_NumLocalObjects()
{
	return 0;
}

int32 ADTGameMode::DT_NumLoadedObjects()
{
	return 0;
}

int32 ADTGameMode::DT_NumActiveObjects()
{
	return 0;
}

int32 ADTGameMode::DT_NumVisibleObjects()
{
	return 0;
}

bool ADTGameMode::DT_InCluster()
{
	return false;
}

void ADTGameMode::DT_Ready()
{
}

bool ADTGameMode::DT_Running()
{
	return false;
}

int ADTGameMode::DT_NumNodes()
{
	return -1;
}

int ADTGameMode::DT_NumWorkers()
{
	return -1;
}

bool ADTGameMode::DT_IsWorker(int nodeID)
{
	return false;
}

int ADTGameMode::DT_ClusterID()
{
	return -1;
}

float ADTGameMode::DT_SpaceSize()
{
	return 0.0f;
}

float ADTGameMode::DT_MaxSize()
{
	return 0.0f;
}

float ADTGameMode::DT_Time()
{
	return FPlatformTime::Seconds() - ADTGameMode::StartupTime;
}

float ADTGameMode::DT_UpdateRate()
{
	return 0.0f;
}

void ADTGameMode::DT_SwitchOnRole(ADTGameMode* Target, DTRoleEnum& Branches)
{
	Branches = DTRoleEnum::Standalone;
}

bool ADTGameMode::DT_HasAuthority(AActor* Target)
{
	return Target->HasAuthority();
}

bool ADTGameMode::DT_IsDATActor(AActor* Target)
{
	ADTActor* a = Cast<ADTActor>(Target);
	if(a != NULL)
		return true;
	else
	{
		ADTPawn* p = Cast<ADTPawn>(Target);
		if(p != NULL)
			return true;
		else
		{
			ADTCharacter* c = Cast<ADTCharacter>(Target);
			if(c != NULL)
				return true;
		}
	}

	return false;
}

void ADTGameMode::DT_GetWorkersForType(FString TypeName, TArray<int32>& Workers, bool& Success)
{
	Success = false;
}

void ADTGameMode::DT_Move(DTType* typ)
{
}

void ADTGameMode::DT_MoveView(DTType* typ)
{
}

void ADTGameMode::DT_PrepareMessage(int MessageLength)
{
	if(outData != NULL)
	{
		if(MessageLength == outSize)
		{
			outCur = 0;
			return;
		}

		delete outData;
	}

	outSize = MessageLength;
	if(outSize > 0)
	{
		outData = new byte[outSize];
		outCur = 0;
	}
	else
		outData = NULL;
}

void ADTGameMode::DT_PrepareSpawn(int DataLength)
{
	DT_PrepareMessage(DataLength);
}

void ADTGameMode::DT_PrepareMultiSpawn(int DataLength, int SpawnCount)
{
	spawnCount = SpawnCount;
	spawnSize = DataLength;

	DT_PrepareMessage(spawnCount * spawnSize);
}

void ADTGameMode::DT_ReadData(byte* data, int len)
{
	memcpy(data, inData + inCur, len);
	inCur += len;
}

void ADTGameMode::DT_WriteData(const byte* data, int len)
{
	memcpy(outData + outCur, data, len);
	outCur += len;
}

void ADTGameMode::DT_ReadString(FString& String)
{
	int strLen;

	DT_ReadObject<int>(strLen);

	char* str = new char[strLen + 1];
	DT_ReadData((byte*) str, strLen);
	str[strLen] = '\0';

	String = str;
	delete str;
}

int ADTGameMode::DT_StringLen(const FString& String) const
{
	return sizeof(int) + String.Len();
}

void ADTGameMode::DT_WriteString(const FString& String)
{
	char* stringStr = TCHAR_TO_ANSI(*String);
	int stringStrLen = String.Len();

	DT_WriteObject<int>(stringStrLen);
	DT_WriteData((byte*) stringStr, stringStrLen);
}

void ADTGameMode::DT_PrintString(const FString& String)
{
#ifndef DAT_CLIENT_MODE
	UE_LOG(LogTemp, Log, TEXT("%s"), *String);
#endif
}

void ADTGameMode::DT_SendMasterMessage(int to, int code)
{
	inData = outData;
	inSize = outCur;
	inCur = 0;

	DT_HandleMasterMessage(0, code);
	inData = NULL;
	inSize = 0;
}

void ADTGameMode::DT_SendMasterMessage(int code)
{
	inData = outData;
	inSize = outCur;
	inCur = 0;

	DT_HandleMasterMessage(0, code);
	inData = NULL;
	inSize = 0;
}

void ADTGameMode::DT_SendGlobalMessage(int code)
{
	inData = outData;
	inSize = outCur;
	inCur = 0;

	DT_HandleMasterMessage(0, code);
	inData = NULL;
	inSize = 0;
}

void ADTGameMode::HandleSpawn(int entityType, byte* data, int dataSz)
{
	inData = data;
	inSize = dataSz;
	inCur = 0;

	DT_HandleSpawn(entityType);
	inData = NULL;
	inSize = 0;
}

void ADTGameMode::DT_SpawnOnWorker(int workerID, int entityType)
{
	HandleSpawn(entityType, outData, outCur);
	outCur = 0;
}

void ADTGameMode::DT_SpawnMultiOnWorker(int workerID, int entityType)
{
	for(int i=0;i<spawnCount;i++)
		HandleSpawn(entityType, outData + (i * spawnSize), spawnSize);

	outCur = 0;
	spawnCount = 0;
	spawnSize = 0;
}

void ADTGameMode::DT_DistributedSpawn(int entityType)
{
	HandleSpawn(entityType, outData, outCur);
	outCur = 0;
}

void ADTGameMode::DT_DistributedMultiSpawn(int entityType)
{
	for(int i=0;i<spawnCount;i++)
		HandleSpawn(entityType, outData + (i * spawnSize), spawnSize);

	outCur = 0;
	spawnCount = 0;
	spawnSize = 0;
}

void ADTGameMode::DT_HandleSpawn(int entityType)
{
}

DTEntity* ADTGameMode::DT_FindEntity(EntityAddr eAddr)
{
	return NULL;
}

AActor* ADTGameMode::DT_FindActor(EntityAddr eAddr)
{
	return NULL;
}
