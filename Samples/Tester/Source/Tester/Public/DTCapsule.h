/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#pragma once

#include "Tester.h"
#include "DTActor.h"
#include "DTCapsule.generated.h"

class EDTCapsule;

/**
 * 
 */
UCLASS()
class TESTER_API ADTCapsule : public ADTActor
{
	GENERATED_UCLASS_BODY()
	
public:

	// The base color of the cube
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated, Meta = (ExposeOnSpawn = true))
	FLinearColor BaseColor;
	
	// Initialize actor (DAT)
	virtual void DT_Initialize() override;

	// Duplicate remote actor's initial settings (DAT)
	virtual void DT_Duplicate() override;

	// Override this to provide DAT with your actor's type
	virtual int DT_GetType() const override { return EntType_Capsule; }	

	virtual void DT_HandleMessage(AActor* From, int MessageType) override;

private:
	EDTCapsule* entity;
};

// This allows us to do our own replication in the DAT cluster
class EDTCapsule : public DTEntity
{
public:

	// Loads the data for our projectile
	virtual void Load() override;

	// Stores the data for our projectile
	virtual void Store() override;

	FLinearColor color;
};

