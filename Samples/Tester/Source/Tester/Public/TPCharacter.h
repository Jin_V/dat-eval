/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#pragma once

#include "Tester.h"
#include "DTActor.h"
#include "TPCharacter.generated.h"

class ETPCharacter;

enum class ECharacterEvent : uint8
{
	Test1
};

//
// ATPCharacter
//

UCLASS()
class TESTER_API ATPCharacter : public ADTCharacter
{
	GENERATED_UCLASS_BODY()
	
public:

	// Spawn a cube on a worker in the cluster
	UFUNCTION(BlueprintCallable)
	void RemoteSpawnCube(FVector Location, FRotator Rotation, FVector Scale);

	// Spawn a sphere on a worker in the cluster
	UFUNCTION(BlueprintCallable)
	void RemoteSpawnSphere(FVector Location, FRotator Rotation, FVector Scale);

	// Spawn a capsule on a worker in the cluster
	UFUNCTION(BlueprintCallable)
	void RemoteSpawnCapsule(FVector Location, FRotator Rotation, FVector Scale);

	// Trigger the event Test1
	UFUNCTION(BlueprintCallable)
	void TriggerTest1(FVector Location);

	// Handle the event Test1
	UFUNCTION(BlueprintImplementableEvent)
	void HandleTest1(FVector Location);

	// Initialize character (DAT)
	virtual void DT_Initialize() override;

	// Duplicate remote actor's static settings (DAT)
	virtual void DT_Duplicate() override;

	// Reflect loaded data onto components (DAT)
	virtual void DT_Reflect(bool ClusterUpdate) override;

	// Update saved data from components (DAT)
	virtual void DT_Update() override;

	// Override this to provide DAT with your character's type
	virtual int DT_GetType() const override { return EntType_Character; }

	virtual void Tick(float DeltaSeconds) override;

private:
	// Represents a character on the DAT side
	ETPCharacter* entity;

	// Allows us to only update the movement component on change
	byte lastMovMode;
};

// This allows us to do our own replication in the DAT cluster
class ETPCharacter : public DTEntity
{
public:

	// Load the data for our character
	virtual void Load() override;

	// Store the data for our character
	virtual void Store() override;

	// The movement mode that we must replicate
	byte movMode;

	FVector testVector;
};
