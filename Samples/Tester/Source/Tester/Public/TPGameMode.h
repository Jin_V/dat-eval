/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#pragma once

#include "Tester.h"
#include "TPCharacter.h"
#include "DTCube.h"
#include "DTSphere.h"
#include "DTCapsule.h"
#include "DTGameMode.h"
#include "TPGameMode.generated.h"

class TPCharacter;

//
// ATPGameMode
//
UCLASS()
class TESTER_API ATPGameMode : public ADTGameMode
{
	GENERATED_BODY()
	
public:
	ATPGameMode();

	// Blueprint class for the player characters
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ATPCharacter> CharacterBlueprintClass;

	// Blueprint class for the cubes
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ADTCube> CubeBlueprintClass;

	// Blueprint class for the spheres
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ADTSphere> SphereBlueprintClass;

	// Blueprint class for the capsules
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ADTCapsule> CapsuleBlueprintClass;

	// Initialize this game mode (DAT calls this after cluster is ready but before the DT_SetupWorld event)
	virtual void DT_Initialize() override;

	// Workers implement this to handle spawn request from the Master
	virtual void DT_HandleSpawn(int EntityType) override;

	// Spawn an object
	void SpawnObject(int EntType, FVector Location, FRotator Rotation, FVector Scale);

	// Called when a worker should spawn a cube
	UFUNCTION(BlueprintImplementableEvent)
	void HandleSpawnCube(FVector Location, FRotator Rotation, FVector Scale);

	// Called when a worker should spawn a sphere
	UFUNCTION(BlueprintImplementableEvent)
	void HandleSpawnSphere(FVector Location, FRotator Rotation, FVector Scale);

	// Called when a worker should spawn a capsule
	UFUNCTION(BlueprintImplementableEvent)
	void HandleSpawnCapsule(FVector Location, FRotator Rotation, FVector Scale);

	// DAT type object for our player characters
	DTType* playerType;

	// DAT type object for the cubes
	DTType* cubeType;

	// DAT type object for the spheres
	DTType* sphereType;

	// DAT type object for the capsules
	DTType* capsuleType;
};
