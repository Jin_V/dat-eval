/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#include "DTSphere.h"
#include "Components/SphereComponent.h"

ADTSphere::ADTSphere(const FObjectInitializer& ObjectInitializer)
  : ADTActor(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	entity = NULL;
}

void ADTSphere::DT_Initialize()
{
	Super::DT_Initialize();

	// Create a new EDTSphere to represent this actor on the DAT side
	entity = (EDTSphere*) DT_Entity();

	if(entity != NULL)
	{
		// Give DAT the bounding sphere
		USphereComponent* sphere = Cast<USphereComponent>(RootComponent);
		check(sphere != NULL);

		DT_SetBoundingSphere(sphere->GetUnscaledSphereRadius());

		// Get the scale of the actor
		DT_SetScale(GetActorScale3D());

		// We want the full physics effect so we'll keep it on all the time
		DT_SetMaterial(DTMaterialityEnum::Enabled);

		// Initialize our values
		entity->color = BaseColor;
	}
}

void ADTSphere::DT_Duplicate()
{
	Super::DT_Duplicate();

	// Save the EDTSphere that represents this actor on the DAT side
	entity = (EDTSphere*) DT_Entity();

	// Apply the scale to this actor
	SetActorScale3D(DT_Scale());

	// Initialize our state to match the cube we are replicating
	BaseColor = entity->color;
}

void ADTSphere::DT_HandleMessage(AActor* From, int MessageType)
{
	FVector origin;
	float force;

	DT_ReadObject<FVector>(origin);
	DT_ReadObject<float>(force);

	FVector impulse = GetActorLocation() - origin;
	impulse.Normalize();
	impulse = impulse * force;

	Cast<UPrimitiveComponent>(RootComponent)->AddImpulse(impulse, NAME_None, true);
}

void ADTSphere::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ADTSphere, BaseColor);
}

void EDTSphere::Load()
{
	// Load all the core data
	DTEntity::Load();

	// Load our custom data
	DT_ReadObject<FLinearColor>(color);
}

void EDTSphere::Store()
{
	// Store all the core data
	DTEntity::Store();

	// Store our custom data
	DT_WriteObject<FLinearColor>(color);
}
