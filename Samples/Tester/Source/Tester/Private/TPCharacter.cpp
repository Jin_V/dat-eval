/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#include "TPCharacter.h"
#include "TPGameMode.h"
#include "UnrealNetwork.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"

ATPCharacter::ATPCharacter(const FObjectInitializer& ObjectInitializer)
  : ADTCharacter(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	entity = NULL;
}

void ATPCharacter::RemoteSpawnCube(FVector Location, FRotator Rotation, FVector Scale)
{
	Cast<ATPGameMode>(GetWorld()->GetAuthGameMode())->SpawnObject(EntType_Cube, Location, Rotation, Scale);
}

void ATPCharacter::RemoteSpawnSphere(FVector Location, FRotator Rotation, FVector Scale)
{
	Cast<ATPGameMode>(GetWorld()->GetAuthGameMode())->SpawnObject(EntType_Sphere, Location, Rotation, Scale);
}

void ATPCharacter::RemoteSpawnCapsule(FVector Location, FRotator Rotation, FVector Scale)
{
	Cast<ATPGameMode>(GetWorld()->GetAuthGameMode())->SpawnObject(EntType_Capsule, Location, Rotation, Scale);
}

void ATPCharacter::TriggerTest1(FVector Location)
{
	DT_PrepareMessage(sizeof(FVector) + sizeof(float));

	DT_WriteObject<FVector>(Location);
	DT_WriteObject<float>(1000.0f);

	DT_EmitMessage(Location, 500.0f, (int) ECharacterEvent::Test1);
}

void ATPCharacter::DT_Initialize()
{
	Super::DT_Initialize();

	// Create a new ETPCharacter to represent this actor on the DAT side
	entity = (ETPCharacter*) DT_Entity();

	if(entity != NULL)
	{
		// Give DAT the bounding capsule
		UCapsuleComponent* capsule = Cast<UCapsuleComponent>(RootComponent);
		check(capsule != NULL);

		DT_SetBoundingCapsule(capsule->GetScaledCapsuleRadius(), capsule->GetScaledCapsuleHalfHeight(), 0.0f);

		// Override materiality so it's always enabled (character movement requires it)
		DT_SetMaterial(DTMaterialityEnum::Enabled);

		// Set the view parameters (how well the character can see)
		DT_SetVisibilityScalar(0.001f);
	}
}

void ATPCharacter::DT_Duplicate()
{
	Super::DT_Duplicate();

	// Save the ETPCharacter that represents this actor on the DAT side
	entity = (ETPCharacter*) DT_Entity();

	// Get the movement component
	UCharacterMovementComponent* mc = (UCharacterMovementComponent*) GetMovementComponent();

	// Tell the movement component to work even though there is no player input driving it
	mc->bRunPhysicsWithNoController = true;
}

void ATPCharacter::DT_Reflect(bool ClusterUpdate)
{
	if(ClusterUpdate)
	{
		SetActorLocationAndRotation(DT_Location(), DT_Rotation());

		// Get the movement component
		UCharacterMovementComponent* mc = (UCharacterMovementComponent*)GetMovementComponent();

		// Get the velocity of the movement component from DAT
		mc->Velocity = DT_LinearVelocity();

		// If the movement mode from DAT has changed, update the movement component
		if(entity->movMode != lastMovMode)
		{
			mc->SetMovementMode((EMovementMode)entity->movMode);
			lastMovMode = entity->movMode;
		}
	}
}

void ATPCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void ATPCharacter::DT_Update()
{
	Super::DT_Update();

	// Get the movement component
	UCharacterMovementComponent* mc = (UCharacterMovementComponent*) GetMovementComponent();

	// Give DAT the actor's velocity from the movement component
	DT_SetLinearVelocity(mc->Velocity);

	// Give DAT the current movement mode
	entity->movMode = (byte) mc->MovementMode;
}

void ETPCharacter::Load()
{
	// Load all the core data
	DTEntity::Load();

	// Load our custom data
	DT_ReadObject<FVector>(testVector);
	DT_ReadObject<byte>(movMode);
}

void ETPCharacter::Store()
{
	// Store all the core data
	DTEntity::Store();

	// Store our custom data
	DT_WriteObject<FVector>(testVector);
	DT_WriteObject<byte>(movMode);
}
