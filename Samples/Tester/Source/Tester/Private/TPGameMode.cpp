/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#include "TPGameMode.h"


ATPGameMode::ATPGameMode()
{
	// Set intial values for DAT-related variables
	MasterHost = "127.0.0.1:6666";
	UpdateWaitMS = 20;
}

void ATPGameMode::DT_Initialize()
{
	// Create our DAT type(s)
	playerType = DAT_TYPE<EntType_Character, ATPCharacter, ETPCharacter>("Player", 20, CharacterBlueprintClass, DTBehavior::CollisionAndVision);
	cubeType = DAT_TYPE<EntType_Cube, ADTCube, EDTCube>("Cube", 100, CubeBlueprintClass, DTBehavior::CollisionOnly);
	sphereType = DAT_TYPE<EntType_Sphere, ADTSphere, EDTSphere>("Sphere", 100, SphereBlueprintClass, DTBehavior::CollisionOnly);
	capsuleType = DAT_TYPE<EntType_Capsule, ADTCapsule, EDTCapsule>("Capsule", 100, CapsuleBlueprintClass, DTBehavior::CollisionOnly);

	// Tell DAT that we are ready to join the cluster
	DT_Ready();
}

void ATPGameMode::SpawnObject(int EntType, FVector Location, FRotator Rotation, FVector Scale)
{
	// Create the data to send with the spawn
	DT_PrepareSpawn((sizeof(FVector) * 2) + sizeof(FRotator));

	// Write the spawn parameters to the data
	DT_WriteObject<FVector>(Location);
	DT_WriteObject<FRotator>(Rotation);
	DT_WriteObject<FVector>(Scale);

	// Send the spawn request
	DT_DistributedSpawn(EntType);
}

void ATPGameMode::DT_HandleSpawn(int EntityType)
{
	FVector location;
	FRotator rotation;
	FVector scale;

	// Read the spawn parameters from the data
	DT_ReadObject<FVector>(location);
	DT_ReadObject<FRotator>(rotation);
	DT_ReadObject<FVector>(scale);

	switch(EntityType)
	{
	case EntType_Cube:
		HandleSpawnCube(location, rotation, scale);
		break;
	case EntType_Sphere:
		HandleSpawnSphere(location, rotation, scale);
		break;
	case EntType_Capsule:
		HandleSpawnCapsule(location, rotation, scale);
		break;
	default:
		check(false);
	}
}