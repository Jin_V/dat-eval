/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#include "DTCapsule.h"
#include "Components/CapsuleComponent.h"

ADTCapsule::ADTCapsule(const FObjectInitializer& ObjectInitializer)
  : ADTActor(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	entity = NULL;
}

void ADTCapsule::DT_Initialize()
{
	Super::DT_Initialize();

	// Create a new EDTCapsule to represent this actor on the DAT side
	entity = (EDTCapsule*) DT_Entity();

	if(entity != NULL)
	{
		// Give DAT the bounding capsule
		UCapsuleComponent* capsule = Cast<UCapsuleComponent>(RootComponent);
		check(capsule != NULL);

		DT_SetBoundingCapsule(capsule->GetScaledCapsuleRadius(), capsule->GetScaledCapsuleHalfHeight(), 0.0f);

		// Get the scale of the actor
		DT_SetScale(GetActorScale3D());

		// We want the full physics effect so we'll keep it on all the time
		DT_SetMaterial(DTMaterialityEnum::Enabled);

		// Initialize our values
		entity->color = BaseColor;
	}
}

void ADTCapsule::DT_Duplicate()
{
	Super::DT_Duplicate();

	// Save the EDTCapsule that represents this actor on the DAT side
	entity = (EDTCapsule*) DT_Entity();

	// Apply the scale to this actor
	SetActorScale3D(DT_Scale());

	// Initialize our state to match the cube we are replicating
	BaseColor = entity->color;
}

void ADTCapsule::DT_HandleMessage(AActor* From, int MessageType)
{
	FVector origin;
	float force;

	DT_ReadObject<FVector>(origin);
	DT_ReadObject<float>(force);

	FVector impulse = GetActorLocation() - origin;
	impulse.Normalize();
	impulse = impulse * force;

	Cast<UPrimitiveComponent>(RootComponent)->AddImpulse(impulse, NAME_None, true);
}

void ADTCapsule::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ADTCapsule, BaseColor);
}

void EDTCapsule::Load()
{
	// Load all the core data
	DTEntity::Load();

	// Load our custom data
	DT_ReadObject<FLinearColor>(color);
}

void EDTCapsule::Store()
{
	// Store all the core data
	DTEntity::Store();

	// Store our custom data
	DT_WriteObject<FLinearColor>(color);
}
