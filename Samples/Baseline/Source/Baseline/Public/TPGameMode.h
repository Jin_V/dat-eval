/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#pragma once

#include "Baseline.h"
#include "TPCharacter.h"
#include "DTGameMode.h"
#include "TPGameMode.generated.h"

class TPCharacter;

//
// ATPGameMode
//
UCLASS()
class BASELINE_API ATPGameMode : public ADTGameMode
{
	GENERATED_BODY()
	
public:
	ATPGameMode();

	// Blueprint class for the player characters
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ATPCharacter> ATPCharacterBlueprintClass;

	// Initialize this game mode (DAT calls this after cluster is ready but before the DT_SetupWorld event)
	virtual void DT_Initialize() override;

	// DAT type object for our player characters
	DTType* playerType;
};
