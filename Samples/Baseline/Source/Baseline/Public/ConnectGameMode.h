/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#pragma once

#include "GameFramework/GameMode.h"
#include "ConnectGameMode.generated.h"

//
// AConnectGameMode
//
UCLASS()
class BASELINE_API AConnectGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:

	// Hostname for the "open" console command
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString HostName;

	// Port for the "open" console command
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int HostPort;

	virtual void BeginPlay() override;
};
