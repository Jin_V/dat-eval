@copy libzmq-v120-mt-4_0_4.dll WindowsServer\Tester\Binaries\Win64

@start .\WindowsServer\TesterServer.exe -log -PORT=6667 -MasterMode -UseLocalhost -DOTCount 2 -UE4Count 3

@start DOT.exe 127.0.0.1 6666
@start DOT.exe 127.0.0.1 6666

@start .\WindowsServer\TesterServer.exe -PORT=7777 -log -UpdateWaitMS 33.33334 -UseLocalhost -GatewayMode
@start .\WindowsServer\TesterServer.exe -PORT=7778 -log -UpdateWaitMS 33.33334 -UseLocalhost -GatewayMode
@start .\WindowsServer\TesterServer.exe -PORT=7779 -log -UpdateWaitMS 33.33334 -UseLocalhost

