@copy libzmq-v120-mt-4_0_4.dll WindowsServer\Tester\Binaries\Win64

@start .\WindowsServer\TesterServer.exe -log -PORT=6667 -UpdateWaitMS 33.33334 -MasterMode -UseLocalhost -DOTCount 2 -UE4Count 5

@start DOT.exe 127.0.0.1 6666
@start DOT.exe 127.0.0.1 6666

@start .\WindowsServer\TesterServer.exe -PORT=7777 -log -UseLocalhost -GatewayMode
@start .\WindowsServer\TesterServer.exe -PORT=7778 -log -UseLocalhost -GatewayMode
@start .\WindowsServer\TesterServer.exe -PORT=7779 -log -UseLocalhost -Exclude="Sphere;Capsule"
@start .\WindowsServer\TesterServer.exe -PORT=7780 -log -UseLocalhost -Exclude="Cube;Capsule"
@start .\WindowsServer\TesterServer.exe -PORT=7781 -log -UseLocalhost -Exclude="Cube;Sphere"