@copy libzmq-v120-mt-4_0_4.dll WindowsServer\Cubes\Binaries\Win64

@start .\WindowsServer\CubesServer.exe -log -PORT=6667 -UpdateWaitMS 33.33334 -MasterMode -UseLocalhost -DOTCount 2 -UE4Count 4 -FountainGridSizeX 1 -FountainGridSizeY 2 -FountainGridCountX 2 -FountainGridCountY 2 -FountainGridSpacingX 50000 -FountainGridSpacingY 50000 -FountainSpacingX 500 -FountainSpacingY 500

@start DOT.exe 127.0.0.1 6666
@start DOT.exe 127.0.0.1 6666

@start .\WindowsServer\CubesServer.exe -log -PORT=7777 -UseLocalhost -GatewayMode
@start .\WindowsServer\CubesServer.exe -log -PORT=7778 -UseLocalhost -GatewayMode
@start .\WindowsServer\CubesServer.exe -log -PORT=7779 -UseLocalhost
@start .\WindowsServer\CubesServer.exe -log -PORT=7780 -UseLocalhost