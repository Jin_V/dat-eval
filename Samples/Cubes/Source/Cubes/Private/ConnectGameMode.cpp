/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#include "ConnectGameMode.h"
#include "Misc/CommandLine.h"

AConnectGameMode::AConnectGameMode()
{
	HostName = "127.0.0.1";
	HostPort = 7777;
}

void AConnectGameMode::BeginPlay()
{
	const TCHAR* cmdLineStr = FCommandLine::Get();
	if(cmdLineStr != NULL)
	{
		FString inStr;
		int inInt;

		// Get the host name from the command line if it's there
		if(FParse::Value(cmdLineStr, TEXT("HostName"), inStr))
			HostName = inStr.Right(inStr.Len() - 1).TrimQuotes();

		// Get the host port from the command line if it's there
		if(FParse::Value(cmdLineStr, TEXT("HostPort"), inInt))
			HostPort = inInt;
	}

	Super::BeginPlay();
}
