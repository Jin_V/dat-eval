/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#include "DTCube.h"
#include "Components/BoxComponent.h"

DTType* ADTCube::dtType;

AInstanceManager* ADTCube::InstanceManager[MAX_CUBE_COLORS] = { NULL, NULL, NULL, NULL };

ADTCube::ADTCube()
  : ADTActor()
{
	PrimaryActorTick.bCanEverTick = true;
	ColorID = 0;
	InstMgr = NULL;
	HasInstance = false;
}

void ADTCube::BeginPlay()
{
	Super::BeginPlay();

	CreateInstance();
}

void ADTCube::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	DestroyInstance();
}

void ADTCube::DT_Show()
{
	SetActorHiddenInGame(false);
	CreateInstance();
}

void ADTCube::DT_Hide()
{
	SetActorHiddenInGame(true);
	DestroyInstance();
}

void ADTCube::CreateInstance()
{
	if(!IsRunningDedicatedServer() && !HasInstance)
	{
		if(InstanceManager[ColorID] == NULL)
		{
			UWorld* world = GetWorld();
			InstMgr = InstanceManager[ColorID] = world->SpawnActor<AInstanceManager>(AInstanceManager::StaticClass());
			InstMgr->SetInstancedMesh(CubeMesh);
			check(CubeMaterials.Num() > ColorID);
			InstMgr->SetInstancedMaterial(0, CubeMaterials[ColorID]);
		}
		else
			InstMgr = InstanceManager[ColorID];

		InstMgr->AddInstance(this);
		HasInstance = true;
	}
}

void ADTCube::DestroyInstance()
{
	if(!IsRunningDedicatedServer() && HasInstance)
	{
		InstMgr->RemoveInstance(this);
		HasInstance = false;

		if(InstMgr->GetInstanceCount() == 0)
		{
			InstMgr->Destroy();
			InstanceManager[ColorID] = NULL;
		}
	}
}

void ADTCube::DT_Initialize()
{
	Super::DT_Initialize();

	// Create a new CubeEntity to represent this actor on the DAT side
	entity = (CubeEntity*) DT_Entity();
	
	if(entity != NULL)
	{
		// Give DAT the extents of the cube
		FVector ext = ((UBoxComponent*)RootComponent)->GetUnscaledBoxExtent();
		DT_SetBoundingBox(FBox(-ext, ext));

		// Set the scale of the actor
		DT_SetScale(GetActorScale3D());

		// Keep the cube solid on remote servers/clients
		//DT_SetMaterial(DTMaterialityEnum::Enabled);

		// Initialize our values
		entity->colorID = ColorID;
	}
}

void ADTCube::DT_Duplicate()
{
	Super::DT_Duplicate();

	// Save the CubeEntity that represents this actor on the DAT side
	entity = (CubeEntity*) DT_Entity();

	// Apply the scale to this actor
	SetActorScale3D(DT_Scale());

	// Initialize our state to match the cube we are replicating
	ColorID = entity->colorID;
}

void ADTCube::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ADTCube, ColorID);
}

void CubeEntity::Load()
{
	// Load all the core data
	DTEntity::Load();

	// Load our custom data
	DT_ReadObject<int32>(colorID);
}

void CubeEntity::Store()
{
	// Store all the core data
	DTEntity::Store();

	// Store our custom data
	DT_WriteObject<int32>(colorID);
}
