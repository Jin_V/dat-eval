/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#include "DTPlayerPawn.h"

DTType* ADTPlayerPawn::dtType;

ADTPlayerPawn::ADTPlayerPawn()
  : ADTPawn()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ADTPlayerPawn::DT_Initialize()
{
	Super::DT_Initialize();

	// Create a new PlayerPawnEntity to represent this actor on the DAT side
	entity = (PlayerPawnEntity*) DT_Entity();

	if(entity != NULL)
	{
		// Set the bounding sphere of the actor
		DT_SetBoundingSphere(GetSimpleCollisionRadius());

		// We use acceleration to improve smoothing so we keep players enabled
		DT_SetMaterial(DTMaterialityEnum::Enabled);
	}
}

void ADTPlayerPawn::DT_Duplicate()
{
	Super::DT_Duplicate();

	// Save the PlayerPawnEntity that represents this actor on the DAT side
	entity = (PlayerPawnEntity*) DT_Entity();
}

void ADTPlayerPawn::DT_Reflect(bool ClusterUpdate)
{
	Super::DT_Reflect(ClusterUpdate);

	// Get the thrust vector from DAT
	ThrustVector = entity->thrustVector;
}

void ADTPlayerPawn::DT_Update()
{
	Super::DT_Update();

	// Give the thrust vector to DAT
	entity->thrustVector = ThrustVector;
}

void ADTPlayerPawn::FellOutOfWorld(const class UDamageType & dmgType)
{
	// We override this so the player isn't destroyed when it goes below Kill-Z
}

void ADTPlayerPawn::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ADTPlayerPawn, ThrustVector);
}

void PlayerPawnEntity::Load()
{
	// Load all the core data
	DTEntity::Load();

	// Load our custom data
	DT_ReadObject<FVector>(thrustVector);
}

void PlayerPawnEntity::Store()
{
	// Store all the core data
	DTEntity::Store();

	// Store our custom data
	DT_WriteObject<FVector>(thrustVector);
}

