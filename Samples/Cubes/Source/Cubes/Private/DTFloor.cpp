/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#include "DTFloor.h"
#include "Components/BoxComponent.h"


DTType* ADTFloor::dtType;

ADTFloor::ADTFloor()
  : ADTActor()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ADTFloor::DT_Initialize()
{
	Super::DT_Initialize();

	// Create a new FloorEntity to represent this actor on the DAT side
	entity = (FloorEntity*) DT_Entity();

	if(entity != NULL)
	{
		// Give DAT the extents of the cube
		FVector ext = ((UBoxComponent*)RootComponent)->GetUnscaledBoxExtent();
		DT_SetBoundingBox(FBox(-ext, ext));

		// Set the scale of the actor
		DT_SetScale(GetActorScale3D());

		DT_SetLocation(GetActorLocation());
		DT_SetRotation(GetActorRotation());

		// Initialize our values
		entity->dimensions = Dimensions;
	}
}

void ADTFloor::DT_Duplicate()
{
	Super::DT_Duplicate();

	// Save the FloorEntity that represents this actor on the DAT side
	entity = (FloorEntity*) DT_Entity();

	// Apply the scale to this actor
	SetActorScale3D(DT_Scale());

	SetActorLocationAndRotation(DT_Location(), DT_Rotation());

	// Initialize our state to match the cube we are replicating
	Dimensions = entity->dimensions;
}

void ADTFloor::DT_Reflect(bool ClusterUpdate)
{
	// We override this and don't call the parent's reflect because the floor is static
}

void ADTFloor::DT_Update()
{
	// We override this and don't call the parent's update because the floor is static
}

void ADTFloor::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ADTFloor, Dimensions);
}

void FloorEntity::Load()
{
	// Load all the core data
	DTEntity::Load();

	// Load our custom data
	DT_ReadObject<FVector>(dimensions);
}

void FloorEntity::Store()
{
	// Store all the core data
	DTEntity::Store();

	// Store our custom data
	DT_WriteObject<FVector>(dimensions);
}


