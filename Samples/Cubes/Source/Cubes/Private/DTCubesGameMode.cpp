/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#include "DTCubesGameMode.h"

// The message that nodes pass to their peers when they spawn a fountain
#define DTMSG_FOUNTAIN_SPAWNED DT_MESSAGE(1)

ADTCubesGameMode::ADTCubesGameMode()
{
	// Set intial values for DAT-related variables
	MasterHost = "127.0.0.1:6666";
	FountainSpacingX = 1000.0f;
	FountainSpacingY = 1000.0f;
	FountainGridSizeX = 1;
	FountainGridSizeY = 1;
	FountainGridSpacingX = 40000.0f;
	FountainGridSpacingY = 40000.0f;
	FountainGridCountX = 1;
	FountainGridCountY = 1;
	CubesPerSecond = 5;
}

void ADTCubesGameMode::BeginPlay()
{
	// Get values for DAT-related variables from command line parameters
	const TCHAR* cmdLineStr = FCommandLine::Get();
	if(cmdLineStr != NULL)
	{
		int32 inInt;
		float inFloat;

		if(FParse::Value(cmdLineStr, TEXT("FountainSpacingX"), inFloat))
			FountainSpacingX = inFloat;

		if(FParse::Value(cmdLineStr, TEXT("FountainSpacingY"), inFloat))
			FountainSpacingY = inFloat;

		if(FParse::Value(cmdLineStr, TEXT("FountainGridSizeX"), inInt))
			FountainGridSizeX = inInt;

		if(FParse::Value(cmdLineStr, TEXT("FountainGridSizeY"), inInt))
			FountainGridSizeY = inInt;

		if(FParse::Value(cmdLineStr, TEXT("FountainGridSpacingX"), inFloat))
			FountainGridSpacingX = inFloat;

		if(FParse::Value(cmdLineStr, TEXT("FountainGridSpacingY"), inFloat))
			FountainGridSpacingY = inFloat;

		if(FParse::Value(cmdLineStr, TEXT("FountainGridCountX"), inInt))
			FountainGridCountX = inInt;

		if(FParse::Value(cmdLineStr, TEXT("FountainGridCountY"), inInt))
			FountainGridCountY = inInt;

		if(FParse::Value(cmdLineStr, TEXT("CubesPerSecond"), inInt))
			CubesPerSecond = inInt;
	}

	Super::BeginPlay();
}

void ADTCubesGameMode::DT_Initialize()
{
	// Create our DAT types
	playerPawnType = DAT_TYPE<EntType_Player, ADTPlayerPawn, PlayerPawnEntity>("Player", 10, PlayerPawnBlueprintClass, DTBehavior::CollisionAndVision);
	cubeType = DAT_TYPE<EntType_Cube, ADTCube, CubeEntity>("Cube", 500, CubeBlueprintClass, DTBehavior::CollisionOnly);
	floorType = DAT_TYPE<EntType_Floor, ADTFloor, FloorEntity>("Floor", 100, FloorBlueprintClass, DTBehavior::CollisionOnly);

	// Tell DAT that we are ready to join the cluster
	DT_Ready();
}

void ADTCubesGameMode::SpawnFountain(FVector Location, FRotator Rotation, int32 ColorID)
{
	// Create the data to send with the spawn
	DT_PrepareSpawn(sizeof(FVector) + sizeof(FRotator) + sizeof(int32));

	// Write the spawn parameters to the data
	DT_WriteObject<FVector>(Location);
	DT_WriteObject<FRotator>(Rotation);
	DT_WriteObject<int32>(ColorID);

	// Send the spawn request (we spawn a floor but it really spawns a fountain -- with a floor)
	DT_DistributedSpawn(EntType_Floor);
}

void ADTCubesGameMode::DT_HandleSpawn(int EntityType)
{
	// We are spawning fountains, which aren't only floors, but it works so there you go
	check(EntityType == EntType_Floor);

	FVector location;
	FRotator rotation;
	int32 colorID;

	// Read the spawn parameters from the data
	DT_ReadObject<FVector>(location);
	DT_ReadObject<FRotator>(rotation);
	DT_ReadObject<int32>(colorID);

	// Handle this spawn in the blueprint
	HandleSpawnFountain(location, rotation, colorID);
}

void ADTCubesGameMode::DeclareFountainSpawned(FVector Location, FRotator Rotation, FVector FloorDimensions)
{
	// Create the data to send with the spawn
	DT_PrepareMessage((sizeof(FVector) * 2) + sizeof(FRotator));

	// Write the spawn parameters to the data
	DT_WriteObject<FVector>(Location);
	DT_WriteObject<FRotator>(Rotation);
	DT_WriteObject<FVector>(FloorDimensions);

	// Send a message to all nodes indicating a fountain was spawned and where
	DT_SendGlobalMessage(DTMSG_FOUNTAIN_SPAWNED);
}

void ADTCubesGameMode::DT_HandleMasterMessage(int FromNodeID, int MessageType)
{
	// Set up the variables so the read macros work
	if(MessageType == DTMSG_FOUNTAIN_SPAWNED)
	{
		FVector location;
		FRotator rotation;
		FVector floorDimensions;

		// Read the spawn location from the data
		DT_ReadObject<FVector>(location);
		DT_ReadObject<FRotator>(rotation);
		DT_ReadObject<FVector>(floorDimensions);

		// Handle this spawn notification in the blueprint
		HandleFountainSpawned(location, rotation, floorDimensions);
	}
}



