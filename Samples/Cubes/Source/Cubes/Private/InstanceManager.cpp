// Fill out your copyright notice in the Description page of Project Settings.

#include "InstanceManager.h"


// Sets default values
AInstanceManager::AInstanceManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PostPhysics;

	HISMC = CreateDefaultSubobject<UHierarchicalInstancedStaticMeshComponent>(TEXT("HierarchicalInstancedStaticMeshComponent"));
	if(HISMC != NULL)
		SetRootComponent(HISMC);
}

// Called when the game starts or when spawned
void AInstanceManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AInstanceManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(HISMC != NULL)
	{
		AActor* a;
		int num = Actors.Num();
		if(num > 0)
		{
			check(num == HISMC->GetInstanceCount());
			int end = num - 1;

			for(int i = 0; i < end; i++)
			{
				a = Actors[i];
				if(IsValid(a))
					HISMC->UpdateInstanceTransform(i, a->GetActorTransform(), true, false);
			}

			a = Actors[end];
			if(IsValid(a))
				HISMC->UpdateInstanceTransform(end, a->GetActorTransform(), true, true);
		}
	}
}

void AInstanceManager::SetInstancedMesh(UStaticMesh* Mesh)
{
	if((HISMC == NULL) || (Mesh == NULL))
		return;

	HISMC->SetStaticMesh(Mesh);
	HISMC->SetSimulatePhysics(false);
	HISMC->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AInstanceManager::SetInstancedMaterial(int32 ElementIndex, UMaterialInterface* Material)
{
	if((HISMC == NULL) || (Material == NULL))
		return;

	HISMC->SetMaterial(ElementIndex, Material);
}

void AInstanceManager::AddInstance(AActor* Actor)
{
	if((HISMC == NULL) || (Actor == NULL))
		return;

	int32 index = Actors.Add(Actor);
	int32 iindex = HISMC->AddInstance(Actor->GetActorTransform());
	check(index == iindex);

	Map.Add(Actor, index);
}

void AInstanceManager::RemoveInstance(AActor* Actor)
{
	if((HISMC == NULL) || (Actor == NULL))
		return;

	int32 Index = Map.FindAndRemoveChecked(Actor);
	int32 end = Actors.Num();

	check(Index < Actors.Num());
	check(end == HISMC->GetInstanceCount());

	Actors.RemoveAtSwap(Index);
	bool b = HISMC->RemoveInstance(Index);
	check(b);

	if(Index < end - 1)
	{
		AActor* a = Actors[Index];
		check((a != NULL) && (a != Actor));
		Map.Add(a, Index);
	}
}

int32 AInstanceManager::GetInstanceCount()
{
	if(HISMC == NULL)
		return 0;

	return HISMC->GetInstanceCount();
}