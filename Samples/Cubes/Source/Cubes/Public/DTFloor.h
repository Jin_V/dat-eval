/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#pragma once

#include "Cubes.h"
#include "DTActor.h"
#include "DTFloor.generated.h"

class FloorEntity;

/**
 * 
 */
UCLASS()
class CUBES_API ADTFloor : public ADTActor
{
	GENERATED_BODY()

public:
	ADTFloor();
	
	// The dimensions of the floor
	UPROPERTY(BlueprintReadWrite, Replicated, Meta = (ExposeOnSpawn = true))
	FVector Dimensions;

	// Initialize actor (DAT)
	virtual void DT_Initialize() override;

	// Duplicate remote actor's initial settings (DAT)
	virtual void DT_Duplicate() override;

	// Reflects loaded data onto components (DAT)
	virtual void DT_Reflect(bool ClusterUpdate) override;

	// Updates saved data from components (DAT)
	virtual void DT_Update() override;

	// DAT type object for our floors
	static DTType* dtType;

	// Override this to provide DAT with your actor's type
	virtual int DT_GetType() const override { return EntType_Floor; }	

private:
	FloorEntity* entity;
};

// This allows us to do our own replication in the DAT cluster
class FloorEntity : public DTEntity
{
public:

	// Loads the data for our projectile
	virtual void Load() override;

	// Stores the data for our projectile
	virtual void Store() override;

	FVector dimensions;
};
