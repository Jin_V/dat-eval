/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#pragma once

#include "Cubes.h"
#include "DTGameMode.h"
#include "DTPlayerPawn.h"
#include "DTCube.h"
#include "DTFloor.h"
#include "DTCubesGameMode.generated.h"

/**
 * 
 */
UCLASS()
class CUBES_API ADTCubesGameMode : public ADTGameMode
{
	GENERATED_BODY()
	
public:
	ADTCubesGameMode();

	// Blueprint class for the player characters
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ADTPlayerPawn> PlayerPawnBlueprintClass;

	// Blueprint class for the cubes
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ADTCube> CubeBlueprintClass;

	// Blueprint class for the floors
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ADTFloor> FloorBlueprintClass;

	// The space between fountains in the X direction (Master node uses this)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float FountainSpacingX;

	// The space between fountains in the Y direction (Master node uses this)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float FountainSpacingY;

	// The fountain grid size X (Master node uses this)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int FountainGridSizeX;

	// The fountain grid size Y (Master node uses this)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int FountainGridSizeY;

	// The space between fountain grids in the X direction (Master node uses this)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float FountainGridSpacingX;

	// The space between fountain grids in the Y direction (Master node uses this)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float FountainGridSpacingY;

	// The nomber of fountain grids along the X axis (Master node uses this)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int FountainGridCountX;

	// The nomber of fountain grids along the Y axis (Master node uses this)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int FountainGridCountY;

	// The number of cubes to spawn per second for each fountain (Master node uses this)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int CubesPerSecond;

	// We override BeginPlay so we can get command-line parameters
	virtual void BeginPlay() override;

	// Called by Master to spawn a fountain on a worker
	UFUNCTION(BlueprintCallable, Category = "DTCubesGameMode")
	void SpawnFountain(FVector Location, FRotator Rotation, int32 ColorID);

	// Called when a worker should spawn a fountain
	UFUNCTION(BlueprintImplementableEvent)
	void HandleSpawnFountain(FVector Location, FRotator Rotation, int32 ColorID);

	UFUNCTION(BlueprintCallable, Category = "DTCubesGameMode")
	void DeclareFountainSpawned(FVector Location, FRotator Rotation, FVector FloorDimensions);

	// Called when a cube has been spawned
	UFUNCTION(BlueprintImplementableEvent)
	void HandleFountainSpawned(FVector Location, FRotator Rotation, FVector FloorDimensions);

	// Initialize this game mode (DAT calls this after cluster is ready but before the DT_SetupWorld event)
	virtual void DT_Initialize() override;

	// Workers implement this to handle spawn request from the Master
	virtual void DT_HandleSpawn(int EntityType) override;

	// Handle messages that come over the master channel
	virtual void DT_HandleMasterMessage(int FromNodeID, int MessageType) override;

	// DAT type object for our player characters
	DTType* playerPawnType;

	// DAT type object for cubes
	DTType* cubeType;	

	// DAT type object for floors
	DTType* floorType;	
};
