/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#pragma once

#include "Cubes.h"
#include "DTActor.h"
#include "InstanceManager.h"
#include "DTCube.generated.h"

class CubeEntity;

#define MAX_CUBE_COLORS 4

/**
 * 
 */
UCLASS()
class CUBES_API ADTCube : public ADTActor
{
	GENERATED_BODY()
	
public:
	ADTCube();
	
	// The color ID of the cube
	UPROPERTY(BlueprintReadWrite, Replicated, Meta = (ExposeOnSpawn = true))
	int32 ColorID;

	// Initialize actor (DAT)
	virtual void DT_Initialize() override;

	// Duplicate remote actor's initial settings (DAT)
	virtual void DT_Duplicate() override;

	// DAT type object for our cubes
	static DTType* dtType;

	// Override this to provide DAT with your actor's type
	virtual int DT_GetType() const override { return EntType_Cube; }	

	// Get Hierarchical Instanced Static Mesh (HISM) Manager
	UFUNCTION(BlueprintCallable, Meta = (HidePin = "Target", DefaultToSelf = "Target"))
	static AInstanceManager* GetInstanceManager(int32 ColorIndex) { return InstanceManager[ColorIndex]; }

	// Show the actor when DAT tells us to (we do this in C++ because of the ISMs)
	virtual void DT_Show() override;

	// Hide the actor when DAT tells us to (we do this in C++ because of the ISMs)
	virtual void DT_Hide() override;

	// Creates the ISM if there isn't one
	void CreateInstance();

	// Destroys the ISM if there is one
	void DestroyInstance();

	// The Static Mesh to use for HISM
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMesh* CubeMesh;

	// The Materials to use for HISM
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<UMaterialInterface*> CubeMaterials;

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

private:
	CubeEntity* entity;

	UPROPERTY()
	AInstanceManager* InstMgr;
	bool HasInstance;

	static AInstanceManager* InstanceManager[MAX_CUBE_COLORS];
};

// This allows us to do our own replication in the DAT cluster
class CubeEntity : public DTEntity
{
public:

	// Loads the data for our projectile
	virtual void Load() override;

	// Stores the data for our projectile
	virtual void Store() override;

	int32 colorID;
};

