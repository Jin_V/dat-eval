// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "InstanceManager.generated.h"

UCLASS()
class CUBES_API AInstanceManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInstanceManager();

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	UHierarchicalInstancedStaticMeshComponent* HISMC;

	// Hierarchical Instanced Static Mesh stuff
	UFUNCTION(BlueprintCallable)
	void SetInstancedMesh(UStaticMesh* Mesh);

	UFUNCTION(BlueprintCallable)
	void SetInstancedMaterial(int32 ElementIndex, UMaterialInterface* Material);

	UFUNCTION(BlueprintCallable)
	void AddInstance(AActor* Actor);

	UFUNCTION(BlueprintCallable)
	void RemoveInstance(AActor* Actor);

	UFUNCTION(BlueprintCallable)
	int32 GetInstanceCount();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
private:
	TArray<AActor*> Actors;
	TMap<AActor*, int32> Map;
};
