/*************************************************************************
 * DEMO-ONLY LICENSE
 * ^^^^^^^^^^^^^^^^^
 *
 * Copyright (c) 2011-2019 Nocturnal Innovations LLC
 * All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the
 * property of Nocturnal Innovations LLC and its suppliers, if any.
 * The intellectual and technical concepts contained herein are
 * proprietary to Nocturnal Innovations LLC and may be covered by U.S.
 * and Foreign Patents, patents in process, and are protected by trade
 * secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is permissible but only in the original form in which it was received,
 * and only for the purpose of evaluatating the API of the DAT plugin and
 * understanding how to use the API. All other uses are strictly forbidden
 * unless prior written permission is obtained from Nocturnal Innovations
 * LLC.
 */

#pragma once

#include "Cubes.h"
#include "DTActor.h"
#include "DTPlayerPawn.generated.h"

class PlayerPawnEntity;

/**
 * 
 */
UCLASS()
class CUBES_API ADTPlayerPawn : public ADTPawn
{
	GENERATED_BODY()
	
public:
  // Sets default values for this actor's properties
  ADTPlayerPawn();

	// The thrust vector (allows us to predict velocity for smoothing)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
	FVector ThrustVector;

	// Initialize pawn (DAT)
	virtual void DT_Initialize() override;

	// Duplicate remote actor's initial settings (DAT)
	virtual void DT_Duplicate() override;

	virtual void DT_Reflect(bool ClusterUpdate) override;
	virtual void DT_Update() override;

	// DAT type object for our players
	static DTType* dtType;

	// Gives DAT the type it should use for this actor class
	virtual int DT_GetType() const override { return EntType_Player; }	

	// We override this so the player isn't destroyed when it goes below Kill-Z
	virtual void FellOutOfWorld(const class UDamageType & dmgType) override;

private:
	// Represents this player on the DAT side
	PlayerPawnEntity* entity;
};

// This allows us to do our own replication in the DAT cluster
class PlayerPawnEntity : public DTEntity
{
public:

	// Load the data for our player
	virtual void Load() override;

	// Store the data for our player
	virtual void Store() override;

	FVector thrustVector;
};
